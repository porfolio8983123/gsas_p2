import React, {useState} from 'react';
import Sidebar from './components/Sidebar';
import {BrowserRouter as Router, Routes, Route, Navigate} from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import Allocationrun from './pages/Allocationrun';
import Academy from './pages/Academy';
import Fileupload from './pages/Fileupload';
import Allocation from './pages/Allocation';
import Profile from './pages/Profile';
import Report from './pages/Report';


const App = () => {

  const [isLoggedIn,setIsLoggedIn] = useState(false);

  const handleLogin = () => {
    setIsLoggedIn(true);
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
    window.location.reload();
  }

  return (
    <Router>
      <Routes>
        <Route path = "/" element = {isLoggedIn ? <Navigate to = "/Gyalsung" />:<Login handleAdminLogin = {handleLogin} />} />
        <Route path='/Gyalsung' element = {isLoggedIn?<Sidebar handleLogout = {handleLogout}/>:<Navigate to ="/"/>}>
          <Route index element = {<Dashboard />} />
          <Route path='fileupload' element={<Fileupload/>} />
          <Route path='allocation' element={<Allocation/>} />
          <Route path='academy' element={<Academy/>} />
          <Route path='allocationrun' element={<Allocationrun/>} />
          <Route path = "report" element = {<Report />} />
          <Route path='profile' element={<Profile/>} />
        </Route>
      </Routes>
    </Router>
  )
}

export default App