import React, { useEffect, useState } from 'react';
import { PieChart, pieArcLabelClasses } from '@mui/x-charts/PieChart';
import Slider from '@mui/material/Slider';
import { updateAcademyMark } from '../../store';
import { useDispatch,useSelector } from 'react-redux';

const initialData = [
  { id: 0, value: 50, label: 'Academic Mark' },
  { id: 1, value: 50, label: 'Diversity' },
];

export default function AcademicDiversity() {

  const dispatch = useDispatch();

  const academymark = useSelector((state) => state.configuration);

  useEffect(() => {
    dispatch(updateAcademyMark(initialData[0].value));
  },[]);

  useEffect(() => {
    console.log("academy mark from selector ",academymark);
  },[academymark]);

  const [data, setData] = useState(initialData);

  const totalValue = data.reduce((acc, curr) => acc + curr.value, 0);

  const handleSliderChange = (event, newValue) => {
    const newValueArray = [newValue, 100 - newValue];
    const newData = data.map((item, index) => ({
      ...item,
      value: newValueArray[index],
    }));
    console.log("slider value ",newValue);
    setData(newData);
    console.log("new data ",newData[0].value);
    dispatch(updateAcademyMark(newData[0].value));
  };

  let dataPercentages;
  if (totalValue === 0) {
    dataPercentages = data.map(item => ({
      ...item,
      value: 50,
      color: item.label === 'Diversity' ? '#F04A00' : '#02344F',
    }));
  } else {
    dataPercentages = data.map((item, index) => ({
      ...item,
      value: Math.round((item.value / totalValue) * 100),
      color: index === 0 ? '#F04A00' : '#02344F',
    }));
  }

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <PieChart
          margin={{ left: 100 }}
          series={[
            {
              innerRadius: 30,
              arcLabel: (item) => `${item.value}%`,
              arcLabelMinAngle: 45,
              data: dataPercentages,
              highlightScope: { faded: 'global', highlighted: 'item' },
              faded: { innerRadius: 30, additionalRadius: -30, color: 'gray' },
            },
          ]}
          slotProps={{
            legend: {
              hidden: true // Hide legend
            }
          }}
          sx={{
            [`& .${pieArcLabelClasses.root}`]: {
              fill: 'white',
              fontWeight: 'bold',
            },
          }}
          height={160}
          className="custom-pie-chart"
        />
      </div>
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
        <div style={{ marginRight: '20px', display: 'flex', alignItems: 'center' }}>
          <div style={{ width: '20px', height: '20px', backgroundColor: '#F04A00', marginRight: '5px',borderRadius:'20px' }}></div>
          <span>Academic Mark</span>
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ width: '20px', height: '20px', backgroundColor: '#02344F', marginRight: '5px',borderRadius:'20px' }}></div>
          <span>Diversity</span>
        </div>
      </div>
      <div style={{ width: 250, margin: 'auto',marginTop:"20px" }}>
        <div style={{ height: '10px', background: `linear-gradient(to right, #F04A00 0%, #F04A00 ${data[0].value}%, #02344F ${data[0].value}%, #02344F 100%)`,borderRadius:"10px"}}>
          <Slider
            className='slider'
            size="small"
            aria-labelledby="range-slider"
            value={data[0].value}
            onChange={(event, newValue) => handleSliderChange(event, newValue)}
            valueLabelDisplay="auto"
            min={0}
            max={100}
            step={1}
            marks={[
              { value: 0, label: '0' },
              { value: 10, label: '10' },
              { value: 20, label: '20' },
              { value: 30, label: '30' },
              { value: 40, label: '40' },
              { value: 50, label: '50' },
              { value: 60, label: '60' },
              { value: 70, label: '70' },
              { value: 80, label: '80' },
              { value: 90, label: '90' },
              { value: 100, label: '100' },
            ]}
            sx={{
              '& .MuiSlider-valueLabel': {
                backgroundColor: '#F04A00', // Set background color of value label display
              },
              '& .MuiSlider-thumb': {
                width: 15, // Set width of the thumb
                height: 5, // Set height of the thumb
                backgroundColor: '#D9D9D9', // Set background color of the thumb
                borderRadius: 1, // Set border radius to make it rectangular
              },
            }}
            style={{height:'0px',background:'none'}}
          />
        </div>
      </div>
    </div>
  );
}
