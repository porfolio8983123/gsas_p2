import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { updateFemaleCount } from '../../store';
import { useDispatch,useSelector } from 'react-redux';

export default function GenderIntake({intakeSize}) {

  const dispatch = useDispatch();

  const fc = useSelector((state) => state.configuration);

  const [femaleCount, setFemaleCount] = useState();
  const [maleCount, setMaleCount] = useState();
  const [intakeCapacity,setIntakeCapacity] = useState();

  useEffect(() => {
    console.log("selector from gender ",fc)
    setMaleCount(prevMaleCount => {
      return intakeCapacity - fc.femaleCount;
    });
  },[fc,intakeSize,intakeCapacity])

  useEffect(() => {
    setIntakeCapacity(intakeSize);
  },[intakeSize])

  const handleFemaleChange = (event) => {
    const value = event.target.value;
    if (value === '' || parseInt(value) <= intakeCapacity) {
      console.log("female count ", value);
      setFemaleCount(value);  // Set the female count value
  
      // Calculate and set the male count value
      setMaleCount(prevMaleCount => {
        const parsedValue = parseInt(value);
        return isNaN(parsedValue) ? intakeCapacity - fc.femaleCount : intakeCapacity - parsedValue;
      });

      if (value === '') {
        dispatch(updateFemaleCount(0))
      } else {
        dispatch(updateFemaleCount(parseInt(value)));
      }
    }
  };
  

  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '25ch' },
        display: 'flex',
        flexDirection: 'column', // Display text fields in columns
        justifyContent: 'center',
        alignItems: 'center', // Center the items horizontally
        gap: '50px', // Increase the gap between text fields,
        marginTop: '40px',
      }}
      noValidate
      autoComplete="off"
    >
      <TextField
        label="Female"
        id="outlined-size-small"
        size="small"
        value={fc.femaleCount}
        onChange={handleFemaleChange}
      />
      <TextField
        label="Male"
        id="outlined-size-normal"
        size="small"
        disabled
        value={maleCount}
      />
    </Box>
  );
}
    