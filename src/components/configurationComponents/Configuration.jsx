import GenderIntake from "./GenderIntake"
import AcademicDiversity from "./AcademicDiversity";
import { updateSkilling } from "../../store";
import { useDispatch } from "react-redux";
import { useEffect } from "react";

export const Configuration = ({academySkillingData}) => {

    const dispatch = useDispatch();

    const updateSkillingForAllocation = () => {
        dispatch(updateSkilling(academySkillingData.skillingName));
    }

    useEffect(() => {
        updateSkillingForAllocation();
        console.log("academy skilling name ",academySkillingData.skillingName);
    },[academySkillingData])

    console.log("data from configuration ",academySkillingData);

    return (
        <div className="row p-1">
              <div className="ps-5 pb-3">
                <p style={{fontWeight:'500'}} className="ms-5">Eligible Criteria: Minimum 12 grade</p>
            </div>
            <div className="col-md-6 mb-2">
                <p className="text-center">Academic Mark and Diversity Intake</p>
                <AcademicDiversity />
            </div>
            <div className="col-md-6 mb-2">
                <div className="d-flex justify-content-center align-items-center">
                    <p className="text-center">Academy Capacity: {academySkillingData?.capacity} </p>
                    <p className="text-center ms-4">IntakeSize: {academySkillingData?.intakeSize} </p>
                </div>
                <p className="text-center">Gender Intake</p>
                <GenderIntake intakeSize = {academySkillingData.intakeSize}/>
            </div>
          
        </div>
    )
}