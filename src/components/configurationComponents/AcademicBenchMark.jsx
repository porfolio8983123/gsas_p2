import React, { useState } from 'react';
import { PieChart, pieArcLabelClasses } from '@mui/x-charts/PieChart';
import Slider from '@mui/material/Slider';

const initialData = [
  { id: 0, value: 50, label: 'Academic Bench Mark' },
  { id: 1, value: 50, label: '' },
];

export default function AcademicBenchMark() {

  const [data, setData] = useState(initialData);

  const totalValue = data.reduce((acc, curr) => acc + curr.value, 0);

  const handleSliderChange = (event, newValue) => {
    const newValueArray = [newValue, 100 - newValue];
    const newData = data.map((item, index) => ({
      ...item,
      value: newValueArray[index],
    }));
    setData(newData);
  };

  let dataPercentages;
  if (totalValue === 0) {
    dataPercentages = data.map(item => ({
      ...item,
      value: 50,
      color: item.label === 'Diversity' ? '#FFBF00' : 'rgba(45,156,219,0.15)',
    }));
  } else {
    dataPercentages = data.map((item, index) => ({
      ...item,
      value: (item.value / totalValue) * 100,
      color: index === 0 ? '#FFBF00' : 'rgba(45,156,219,0.15)',
    }));
  }

  const academicBenchmarkPercentage = data.find(item => item.label === 'Academic Bench Mark').value;

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <PieChart
          margin={{ left: 100 }}
          series={[
            {
              innerRadius: 30,
              data: dataPercentages,
              highlightScope: { faded: 'global', highlighted: 'item' },
              faded: { innerRadius: 30, additionalRadius: -30, color: 'gray' },
            },
          ]}
          slotProps={{
            legend: {
              hidden: true // Hide legend
            }
          }}
          sx={{
            [`& .${pieArcLabelClasses.root}`]: {
              fill: 'white',
              fontWeight: 'bold',
            },
          }}
          height={160}
          className="custom-pie-chart"
        >
          <text x="50%" y="50%" textAnchor="middle" dominantBaseline="middle" fontSize="20px">{academicBenchmarkPercentage}%</text>
        </PieChart>
      </div>
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
        <p>Bench Mark</p>
      </div>
      
      <div style={{ width: 250, margin: 'auto' }}>
        <div style={{ height: '10px', background: `linear-gradient(to right, #FFBF00 0%, #FFBF00 ${data[0].value}%, rgba(45,156,219,0.15) ${data[0].value}%,rgba(45,156,219,0.15) 100%)`,borderRadius:"10px" }}>
          <Slider
            className='slider'
            size="small"
            aria-labelledby="range-slider"
            value={data[0].value}
            onChange={(event, newValue) => handleSliderChange(event, newValue)}
            valueLabelDisplay="auto"
            min={0}
            max={100}
            step={10}
            marks={[
              { value: 0, label: '0' },
              { value: 10, label: '10' },
              { value: 20, label: '20' },
              { value: 30, label: '30' },
              { value: 40, label: '40' },
              { value: 50, label: '50' },
              { value: 60, label: '60' },
              { value: 70, label: '70' },
              { value: 80, label: '80' },
              { value: 90, label: '90' },
              { value: 100, label: '100' },
            ]}
            sx={{
              '& .MuiSlider-valueLabel': {
                backgroundColor: '#FFBF00', // Set background color of value label display
              },
              '& .MuiSlider-thumb': {
                width: 15, // Set width of the thumb
                height: 5, // Set height of the thumb
                backgroundColor: '#D9D9D9', // Set background color of the thumb
                borderRadius: 1, // Set border radius to make it rectangular
              },
            }}
            style={{height:'0px'}}
          />
        </div>
      </div>
    </div>
  );
}
