import React from 'react'
import { useState,useEffect } from 'react';
import game from "../assets//img/game-icons_graduate-cap.png";
import vertor from "../assets/img/Vector.png";
import vertor1 from "../assets/img/Vector (1).png";
import vertor2 from "../assets/img/Vector (2).png"
import "../style/Enlisteedetail.css"
import { useSelector } from 'react-redux';

export default function Enlisteedetail({male, female}) {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth - 295 - 30);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth - 295 - 38);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className='mt-4 px-3'>
        <div className='row shadow'>
            <div className='col-md-3 '>
                <div className='row'>
                    <div className='col-md-5 d-flex justify-content-center align-items-center ' style={{backgroundColor:"#F04A00"}}> 
                        <img src={game} alt="Vertor" className='' />
                    </div>
                    <div className='col-md-7 d-flex flex-column p-2' style={{backgroundColor:"#D9D9D9",}}>
                        <div className='text-center'>
                            <h3 style={{color:"#F04A00",fontWeight:"bolder"}}>4</h3>
                        </div>
                        <div className='text-center'>
                            <p style={{fontWeight:'500'}}>Total Academy</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='col-md-3'>
                <div className='row'>
                    <div className='col-md-5 d-flex justify-content-center align-items-center ' style={{backgroundColor:"#F04A00"}}> 
                        <img src={vertor} alt="Vertor" className='' />
                    </div>
                    <div className='col-md-7 d-flex flex-column p-2' style={{backgroundColor:"#D9D9D9",}}>
                        <div className='text-center'>
                            <h3 style={{color:"#F04A00",fontWeight:"bolder"}}>{male + female}</h3>
                        </div>
                        <div className='text-center'>
                            <p style={{fontWeight:'500'}}>Total Enlistees</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='col-md-3'>
                <div className='row'>
                    <div className='col-md-5 d-flex justify-content-center align-items-center ' style={{backgroundColor:"#F04A00"}}> 
                        <img src={vertor1} alt="Vertor" className='' />
                    </div>
                    <div className='col-md-7 d-flex flex-column p-2' style={{backgroundColor:"#D9D9D9",}}>
                        <div className='text-center'>
                            <h3 style={{color:"#F04A00",fontWeight:"bolder"}}>{male}</h3>
                        </div>
                        <div className='text-center'>
                            <p style={{fontWeight:'500'}}>Total Male</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='col-md-3 '>
                <div className='row'>
                    <div className='col-md-5 d-flex justify-content-center align-items-center ' style={{backgroundColor:"#F04A00"}}> 
                        <img src={vertor2} alt="Vertor" className='' />
                    </div>
                    <div className='col-md-7 d-flex flex-column p-2' style={{backgroundColor:"#D9D9D9",}}>
                        <div className='text-center'>
                            <h3 style={{color:"#F04A00",fontWeight:"bolder"}}>{female}</h3>
                        </div>
                        <div className='text-center'>
                            <p style={{fontWeight:'500'}}>Total Female</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}
