import React, { useState, useRef, useEffect } from 'react';
import Header from './../components/Header';
import '../style/AddAcademy.css';
import { BsArrowLeft } from 'react-icons/bs';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useUpdateAcademyMutation } from '../store';
import Select from 'react-select'; // Import react-select
import logo2 from '../assets/img/tooltip/tip.png';
import logo from '../assets/img/tooltip/idea.png';
import { useGetDzongkhagQuery } from '../store';

export default function EditAcademy({ backToAcademy, academy }) {
  const [selectedFile, setSelectedFile] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const [dzongkhag, setDzongkhag] = useState('');
  const [name, setName] = useState('');
  const [capacity, setCapacity] = useState('');
  const fileInputRef = useRef(null);
  const [showTooltip, setShowTooltip] = useState(false);

  useEffect(() => {
    console.log("academy data ",academy);
  },[academy])

  const {data,isLoading:dzongkhagLoading,isError:dzongkhagError} = useGetDzongkhagQuery();

  console.log("all dzongkhag ",data);

  useEffect(() => {
    setName(academy.name);
    setCapacity(academy.capacity);
    setDzongkhag(academy.dzongkhag);
  }, [])

  const [updateAcademy, { isLoading, isError }] = useUpdateAcademyMutation();

  const handleUpdate = () => {
    const id = academy.academyId;
    const data = {
      name,
      capacity,
      dzongkhag
    }

    updateAcademy({ id, data })
      .unwrap()
      .then((response) => {
        console.log(response);
        toast.success(`${response.message}`, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      })
      .catch((error) => {
        console.log(error)
        toast.error(error.error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      })
  }

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleCapacityChange = (e) => {
    setCapacity(e.target.value);
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);

    // Preview the selected image
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewImage(reader.result);
    };
    if (file) {
      reader.readAsDataURL(file);
    } else {
      setPreviewImage(null);
    }
  };

  const handleDrop = (e) => {
    e.preventDefault();
    const file = e.dataTransfer.files[0];
    setSelectedFile(file);

    // Preview the dropped image
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewImage(reader.result);
    };
    if (file) {
      reader.readAsDataURL(file);
    } else {
      setPreviewImage(null);
    }
  };

  const handleDragOver = (e) => {
    e.preventDefault();
  };

  const handleBrowseClick = () => {
    fileInputRef.current.click();
  };

  const handleDzongkhagChange = (selectedOption) => {
    setDzongkhag(selectedOption.value);
  };

  const dzongkhagOptions = data?.data?.map(item => ({
    value: item.dzongkhag,
    label: item.dzongkhag,
  }));

  return (
    <div>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <h4 className='mt-4'>Edit Academy</h4>
      <div className='mt-4 shadow-lg p-3 mb-5 bg-white rounded'>
        <div
          onClick={() => setShowTooltip(!showTooltip)}
          style={{
            position: 'absolute',
            right: '2%',
            border: '1px dotted rgb(248, 244, 244)',
            cursor: 'pointer',
            padding: '8px',
          }}
        >
          <span>
            <img src={logo} alt="logo2" style={{ width: 40, height: 40 }} />
          </span>
          {showTooltip && (
            <span
              className='border border-dark'
              style={{
                zIndex: 1,
                position: 'absolute',
                right: '40%',
                top: '85%',
                width: '300px',
                background: '#fff',
                boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
                borderRadius: '5px',
                padding: '10px',
              }}
            >
              <img src={logo2} alt="logo" style={{ width: 30, height: 30, marginBottom: '8px' }} />
              <h6 className='mt-2' style={{ margin: '0 0 8px' }}>1. Edit details</h6>
              <p style={{ marginBottom: '8px' }}>
                You must provide the valid academy details as the incorrect details will give issues in future.
              </p>
              <h6 style={{ margin: '0 0 8px' }}>2. Image upload</h6>
              <p style={{ marginBottom: '8px' }}>You don't have to upload image </p>
              <p style={{ color: '#F04A00', margin: '0' }}>All the fields must be filled!</p>
            </span>
          )}
        </div>
        <div className='row'>
          <div className='col-md-6 p-5'>
            <div className='spacing'>
              <div className='input1'>
                <label htmlFor="">Name</label>
                <input type="text" name='name' value={name} onChange={handleNameChange} required className='form-control mt-1' />
              </div>
              <div className='input2 mt-4'>
                <div className='row'>
                  <div className='col-md-6 col-12'>
                    <div className='sub-input-1'>
                      <label htmlFor="">Dzongkhag</label>
                      <Select
                        options={dzongkhagOptions}
                        value={{ value: dzongkhag, label: dzongkhag }}
                        onChange={handleDzongkhagChange}
                        className='mt-1'
                      />
                    </div>
                  </div>
                  <div className='col-md-6 col-12'>
                    <div className='sub-input-2'>
                      <label htmlFor="">Capacity</label>
                      <input type="text" name='capacity' value={capacity} onChange={handleCapacityChange} required className='form-control mt-1' />
                    </div>
                  </div>
                  <div className='mt-5'>
                    <div className='mt-5 py-3 px-2'>
                      <div onClick={() => backToAcademy()} style={{ textDecoration: 'none', cursor: 'pointer' }} className='text-dark d-flex'>
                        <BsArrowLeft className='arrow-icon mx-2 h3 mt-2 pt-1' />
                        <p className='text-dark'> Go back to Academy</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='col-md-5 p-5'>
            <div className='spacing'>
              <div className='input1 shadow'>
                <div
                  className='drop-zone'
                  onDrop={handleDrop}
                  onDragOver={handleDragOver}
                  onClick={handleBrowseClick}
                >
                  <div>Drag & drop <br /> a file here, or click to select one</div>
                  <input
                    type='file'
                    className='form-control'
                    onChange={handleFileChange}
                    style={{ display: 'none' }}
                    ref={fileInputRef}
                  />
                </div>
              </div>
            </div>
            <div className='mt-3 shadow preview-image text-center'>
              <div className='preview-box' style={{ backgroundImage: `url(${previewImage})`, height: '200px', backgroundSize: 'cover', backgroundPosition: 'center' }}>
              </div>
            </div>
            <div className='row mt-5'>
              <div className='col-md-6'>
                <button className='btn  form-control arrow-btn border-danger '>Reset</button>
              </div>
              <div className='col-md-6'>
                <button onClick={handleUpdate} className='btn  form-control arrow-btn border-danger' >Update</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
