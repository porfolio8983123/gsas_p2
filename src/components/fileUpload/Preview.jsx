import React, { useEffect, useState } from 'react';
import Papa from 'papaparse';
import Cookies from 'js-cookie';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import FileUploadPreview from './FileUploadPreview';
import { useFetchAcademiesQuery } from "../../store";
import PreviewTable from '../tables/PreviewTable';

function Preview({ setPreviousStep, fileName, selectedFile, setCompleteStep,setActiveStep }) {

  const [token,setToken] = useState('');
  const [isFileSaving,setIsFileSaving] = useState(false);
  const [totalMale,setTotalMale] = useState(0);
  const [totalFemale,setTotalFemale] = useState(0);
  const [genderValue, setGenderValue] = useState("All");
  const [cidError,setCIDError] = useState(false);
  const [columnError,setColumnError] = useState('');

  const [searchCid, setSearchCid] = useState('');
  const [searchGender, setSearchGender] = useState('');
  const [filteredData, setFilteredData] = useState([]);

  const itemsPerPage = 10;
  const [currentPage, setCurrentPage] = useState(0);
  const [csvData, setCsvData] = useState(null);
  const [capacity,setCapacity] = useState(0);
  const [studentName,setStudentName] = useState('');

  const {data,isLoading,isError} = useFetchAcademiesQuery();

  console.log("from preview to get academy ", data);

  useEffect(() => {
      let capacity = 0;
      if (data) {
          data?.academies.map((item) => {
              console.log(item.capacity);
              capacity += item.capacity;
          })
      }
      setCapacity(capacity);
  },[data])

  useEffect(() => {
    if (selectedFile) {
      console.log("selected file from preview ", selectedFile);
      Papa.parse(selectedFile, {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        complete: (result) => {
          setCsvData(result.data);
        },
        error: (error) => {
          console.error('Error parsing CSV ', error);
        },
      });
    }
  }, [selectedFile]);

  useEffect(() => {
    const token = Cookies.get('auth_token');
    setToken(token);
  },[])

  useEffect(() => {
    if (csvData) {
      for (const row of csvData) {
        if ('CID' in row) {
          const cid = row['CID'];
  
          if (cid && (!(cid && cid.toString().length === 11 && /^\d+$/.test(cid)))) {
            setCIDError(true);
            break;
          }
        }
      }
    }
  }, [csvData]);

  useEffect(() => {
    if (csvData) {
      // Get the headers from the first row of the CSV data
      const headers = Object.keys(csvData[0]).map(header => header.trim());
  
      const cidColumnExists = headers.includes('CID');

      if (!cidColumnExists) {
        setColumnError("CID");
      }
  
      const sexColumnExists = headers.includes('SEX');

      if (!sexColumnExists) {
        setColumnError("SEX");
      }

      const nameColumnExists = headers.includes('NAME');

      if (!nameColumnExists) {
        setColumnError("NAME");
      }

      const gewogColumnExists = headers.includes('GEWOG');

      if (!gewogColumnExists) {
        setColumnError("GEWOG");
      }

      const mathColumnExists = headers.includes('MATH');

      if (!mathColumnExists) {
        setColumnError("MATH");
      }

      const bmathColumnExists = headers.includes('BMATH');

      if (!bmathColumnExists) {
        setColumnError("BMATH");
      }

      const englishColumnExists = headers.includes('ENGLISH');

      if (!englishColumnExists) {
        setColumnError("ENGLISH");
      }

      const dzongkhaColumnExists = headers.includes('DZONGKHA');

      if (!dzongkhaColumnExists) {
        setColumnError("DZONGKHA");
      }

      const preferenceoneColumnExists = headers.includes('PREFERENCE1');

      if (!preferenceoneColumnExists) {
        setColumnError("PREFERENCE1")
      }
  

      const preferencetwoColumnExists = headers.includes("PREFERENCE2");

      if (!preferencetwoColumnExists) {
        setColumnError("PREFERENCE2");
      }

      const nopreferenceColumnExist = headers.includes('NOPREFERENCE');

      if (!nopreferenceColumnExist) {
        setColumnError("NOPREFERENCE");
      }

      const hastenfingerColumnExist = headers.includes('HASTENFINGER');

      if (!hastenfingerColumnExist) {
        setColumnError("HASTENFINGER");
      }

      const higereducationColumnExist = headers.includes('HASHIGHEREDUCATION');

      if (!higereducationColumnExist) {
        setColumnError("HASHIGHEREDUCATION");
      }
    }
  }, [csvData]);  

  useEffect(() => {
    if (csvData) {
      const genderCounts = csvData.reduce((counts, row) => {
        const gender = row['SEX']; 
        const cid = row['CID'];
        counts[gender] = (counts[gender] || 0) + 1;

        if (cid && cid.length > 11 || cid.length < 11) {
          setCIDError(true);
        }

        return counts;
      }, {});

      console.log('Gender Counts:', genderCounts);

      setTotalMale(genderCounts.M);
      setTotalFemale(genderCounts.F);
    }

    console.log(totalMale,totalFemale);
  }, [csvData]);

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };

  const startIndex = currentPage * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentData = csvData && csvData.slice(startIndex, endIndex);

  const handleComplete = async () => {

    if (columnError.length > 0) {
      toast.error(`Data missing ${columnError} column`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }

    if (csvData.length > capacity) {
      toast.error(`Your student exceeds the capacity!`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }

    if (cidError) {
      toast.error(`CID does not have a length of 11.`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }

    setIsFileSaving(true);
    const formData = new FormData();
    formData.append('file',selectedFile);
    formData.append('fileName',fileName);

    await fetch('http://localhost:8000/api/fileUpload',{
      method:'POST',
      headers: {
        'Authorization':`Bearer ${token}`
      },
      body:formData,
    })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      if (data.status === 'success') {
        toast.success("File uploaded successfully!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });

        setIsFileSaving(false)
        setTimeout(() => {
          setActiveStep(0)
        },3000)
      }
    })
    .catch((error) => {
      console.log("eror ",error);
      toast.error("Something went wrong!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });

      setIsFileSaving(false)
    })
  };

  return (
    <div style={{
      maxHeight: 'calc(100vh - 100px)',
      overflowY: 'scroll',
      scrollbarWidth: 'thin',
      paddingBottom:'50px'
    }}>
      <div className=' shadow pb-4'>
      <div className='mt-5'>
          <div className=" " >
            <div action="" style={{ backgroundColor: '#02344F',padding:'20px',borderRadius:'10px',color:'white' }}>
              <div className=' mt-2 d-flex justify-content-between'>
                <h6 >File Name: <span  style={{color:"#F04A00"}}>{fileName}</span></h6>
                <h6>Total Enlistee: <span style={{color:"#F04A00"}}>{csvData ? csvData.length : 0}</span></h6>
                <h6>Total Male: <span style={{color:"#F04A00"}}>{totalMale}</span></h6>
                <h6>Total Female: <span style={{color:"#F04A00"}}>{totalFemale}</span></h6>
              </div>
            </div>
          </div>
        </div>
        <div className=''>
          <div>
            {csvData?.length > 0 && 
              <PreviewTable students = {csvData}/>
            }
          </div>
          <div className='d-flex justify-content-between mt-5' style={{marginBottom:20}}>
           <button onClick={setPreviousStep} className='btn' style={{backgroundColor:'#F04A00',color:"white"}}>Previous</button>
        <div>
          {isFileSaving ? (
                <button type="button" className="btn" style={{backgroundColor:'#F04A00',color:"white",paddingLeft:'50px',paddingRight:'50px'}} disabled>
                  <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
              </button>
              ):(
                <button className="btn" onClick={handleComplete} style={{ backgroundColor: '#F04A00', color: 'white' }}>
                  Confirm
                </button>
           )

          }
        </div>  
      </div>
    </div>
     
        
      </div>
      <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
      />

    </div>
    
  );
}

export default Preview;
