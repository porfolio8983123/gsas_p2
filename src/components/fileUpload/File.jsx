import React, { useEffect } from 'react';
import '../../style/fileupload/fileupload.css';
import { BsFiletypeCsv, BsFileEarmarkText } from 'react-icons/bs';
import { useState, useRef } from 'react';
import logo2 from '../../assets/img/tooltip/tip.png';
import logo from '../../assets/img/tooltip/idea.png';
import { useCheckFileNameMutation } from '../../store';

function File({ selectedFile, onFileSelect, setNextStep, setFileUploadedName, fileName, clearInput }) {
  const inputRef = useRef();
  const [isDragOver, setIsDragOver] = useState(false);
  const [noInput, setNoInput] = useState(false);
  const [showTooltip, setShowTooltip] = useState(false);
  const [fileNameError,setFileNameError] = useState(false);
  const [fileNameErrorMessage,setFileNameErrorMessage] = useState('');

  const [checkFileName, {isLoading,isError}] = useCheckFileNameMutation();

  const handleClick = () => {
    setShowTooltip(!showTooltip);
  };

  const winHeight = window.innerHeight;

  const handleDragOver = (event) => {
    event.preventDefault();
    setIsDragOver(true);
  }

  const handleDrop = (event) => {
    event.preventDefault();
    onFileSelect(event.dataTransfer.files[0]);
    setIsDragOver(false);
  }

  const handleDragLeave = () => {
    setIsDragOver(false);
  }

  const handleSubmit = () => {
    if (!selectedFile || !fileName) {
      setNoInput(true);
      setFileNameError(false);
    } else {
      const data = {fileName}
    
    checkFileName(data)
      .unwrap()
      .then((response) => {
        console.log(response);
        if (response.status === 'success') {
          setFileNameError(false);
          setNextStep();
        } else {
          setFileNameError(true);
          setFileNameErrorMessage(response.message);
        }
      })
      .catch((error) => {
        console.log(error)
      })
    }
  }

  useEffect(() => {
    if (selectedFile && fileName) {
      setNoInput(false);
    }
  }, [selectedFile, fileName])

  console.log(selectedFile);
  console.log(fileName);

  return (
    <div>
      <div
        onClick={handleClick}
        style={{
          position: 'absolute',
          left: '90%',
          border: '1px dotted rgb(248, 244, 244)',
          cursor: 'pointer', // Add a pointer cursor on hover
          padding: '8px', // Add some padding for better aesthetics
        }}
      >
        <span>
          <img src={logo} alt="logo2" style={{ width: 40, height: 40 }} />
        </span>
        {showTooltip && (
          <span
            className='border border-dark'
            style={{
              zIndex: 1,
              position: 'absolute',
              right: '40%',
              top: '85%',
              width: '300px',
              background: '#fff', // Add a white background
              boxShadow: '0 0 10px rgba(0, 0, 0, 0.1)', // Add a subtle box shadow
              borderRadius: '5px', // Add rounded corners
              padding: '10px', // Add padding inside the tooltip
            }}
          >
            <img src={logo2} alt="logo" style={{ width: 30, height: 30, marginBottom: '8px' }} />
            <h6 className='mt-2' style={{ margin: '0 0 8px' }}>1. File Upload</h6>
            <p style={{ marginBottom: '8px' }}>
              The file containing enlistee’s details must be .csv extension or csv file type with the following column i.e, CID, name, gender, and gewog.
            </p>
            <h6 style={{ margin: '0 0 8px' }}>2. File name</h6>
            <p style={{ marginBottom: '8px' }}>Please fill the appropriate file name </p>
            <p style={{ color: '#F04A00', margin: '0' }}>All the fields must be filled!</p>
          </span>
        )}
      </div>
      <div className='' style={{display:'flex', justifyContent: 'center',alignItems:'center'}} >
        <div className='fileupload__container'>
          <div className={`file__drag__and__drop__container ${isDragOver ? 'drag__over' : ''}`}
            onDragOver={handleDragOver}
            onDrop={handleDrop}
            onDragLeave={handleDragLeave}
          >
            <div className='file__drag__and__drop__innercontainer'>
              <BsFiletypeCsv size={70} />
              <input type="file" accept=".csv" onChange={(event) => onFileSelect(event.target.files[0])} hidden ref={inputRef} />
              <p className='file__text'>Drag and Drop Or <span
                className='browse__file'
                onClick={() => inputRef.current.click()}
              >Upload</span> a file</p>
            </div>
          </div>
          {selectedFile &&
            <div className='file__name'>
              <BsFileEarmarkText size={30} />
              <div>{selectedFile.name}</div>
            </div>
          }
          <div>
            <label htmlFor="filename">File Name</label>
            <div>
              <input type="text" className='form-control filename' style={{ width: 281, height: 40, paddingLeft: 10 }} id='filename'
                onChange={(e) => setFileUploadedName(e.target.value)}
                value={fileName}
                autoComplete='off'
              />
              {noInput &&
                <p style={{ color: 'red', marginTop: 2 }}>File or filename is missing!</p>
              }
              {fileNameError &&
                <p style={{ color: 'red', marginTop: 2 }}>{fileNameErrorMessage}</p>
              }
            </div>
          </div>

          <div className='button__container'>
            <button type="button" className='btn btn-outline-primary reset__button no-hover'
              onClick={clearInput}
            >Reset</button>
            <button type="button" className='btn btn-outline-primary upload__button'
              onClick={handleSubmit}
            >Next</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default File;
