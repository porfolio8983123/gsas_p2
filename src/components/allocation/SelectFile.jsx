import React, { useState,useEffect } from 'react';
import { FaTrash } from 'react-icons/fa';
import '../../style/AllocationSelection.css';
import Select from 'react-select';
import {useFetchFilesQuery,useDeleteFileMutation} from '../../store';
import { updateData } from '../../store';
import { useDispatch } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { IoIosCloseCircle } from "react-icons/io";
import { ImDropbox } from "react-icons/im";

const SelectFile = ({setNextStep,setPreviousStep}) => {

    const dispatch = useDispatch();

    const {data,error,isLoading, refetch} = useFetchFilesQuery();

    console.log("data from select file ",data,error,isLoading);

    const [fileContent,setFileContent] = useState([]);
    const [Contentheaders,setContentHeaders] = useState([]);
    const [deleteFile,{isLoading:fileDeleteLoading,isError:fileDeleteError}] = useDeleteFileMutation();

    const [selectedOption, setSelectedOption] = useState('');
    const [showForm, setShowForm] = useState(false);
    const [radioChecked, setRadioChecked] = useState(false);
    const [radioCheckedIndex,setRadioCheckedIndex] = useState(null);
    const [showModal, setShowModal] = useState(Array(data?.fileDetails.length).fill(false));

    useEffect(() => {
        refetch();
    },[])

    const notify = () => toast.error('Set the Allocation year!', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        });

    const handleAllocationStart = (index) => {
        if (data && data.fileDetails.length > 0) {
            console.log(data);
            console.log(data.fileDetails.length)
            console.log("year ",selectedOption.value)
            const selectedFile = {
                fileName:data.fileDetails[index].filename,
                fileContent:data.fileDetails[index].fileContent,
                year:selectedOption.value
             }

             if (selectedFile.fileName && selectedFile.fileContent && selectedFile.year) {
                dispatch(updateData(selectedFile));
                setNextStep();
             } else {
                notify();
             }
     
        } else {
            toast.error('File Not Selected!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
                });
        }
    }

    const currentYear = new Date().getFullYear();
    const futureYears = Array.from({ length: 4 }, (_, index) => currentYear + index);
    
    const options = futureYears.map((year) => ({
        value: year,
        label: year.toString(),
    }));

    const customStyles = {
        option: (provided, state) => ({
            ...provided,
            backgroundColor: state.isFocused ? '#F04A00' : 'white',
            color: state.isFocused ? 'white' : 'black',
            '&:hover': {
                backgroundColor: '#F04A00',
                color: 'white',
            },
        }),
    };

    const handleRadioClick = (index) => {
        setRadioChecked(!radioChecked);
        setRadioCheckedIndex(index);
        setShowForm(true);
    };

    const formatDate = (dateString) => {
        const options = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
        };
        return new Date(dateString).toLocaleString('en-US', options);
    };

    const handleSelectChange = (seleted) => {
        setSelectedOption(seleted);
    };

    const handleCloseModal = (index) => {
        const updatedModalState = [...showModal];
        updatedModalState[index] = false;
        setShowModal(updatedModalState);
    };
    
    const handleSubmit2 = (index) => {
        const updatedModalState = [...showModal];
        updatedModalState[index] = true;
        setShowModal(updatedModalState);
    };

    const handleDelete = (id,index) => {
        console.log("file id ",id);
        deleteFile(id)
        .unwrap()
        .then((response) => {
            console.log(response)
            handleCloseModal(index);

            refetch();
        })
        .catch((error) => {
            console.log(error)
            toast.error(error, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
            });
        })
    }

    console.log('selected radio ',radioCheckedIndex)

    return (
        <div className='mt-5'>
            {data?.fileDetails && data?.fileDetails.length > 0 ? 
                (
                    data.fileDetails.map((item,index) => (
                        <div key={index} className='mt-4'>
                            <div className='container-fluid p-3 d-flex'>
                                <div className='d-flex justify-content-center'>
                                    <div className={`custom-radio p-4 ${radioCheckedIndex === index ? 'checked' : ''}`} >
                                        <input type='radio' id={`radioButton-${index}`} name='radioGroup' className='radio-input' />
                                        <label htmlFor={`radioButton-${index}`} className='radio-label' onClick={() => handleRadioClick(index)}></label>
                                    </div>
                                </div>
    
                                <div className='ps-3 pe-3 bg-body rounded d-flex justify-content-between align-items-center w-100 border'>
                                    <div className='d-flex justify-content-center pt-2'>
                                        <p className='ms-3'>{item.filename}</p>
                                        <p className='ms-5'>{formatDate(item.createdAt)}</p>
                                    </div>
                                    <div className='pt-2'>
                                        <button className='px-5 ' style={{ border: 'none', backgroundColor: 'white' }}
                                            onClick={() => {
                                                handleSubmit2(index)
                                            }}
                                        >
                                            <FaTrash className='h4' style={{ color: '#F04A00' }} />
                                        </button>
                                    </div>
                                </div>
                                {showModal[index] && <div className="modal-overlay" onClick={() => {handleCloseModal(index)}} style={{position:'fixed',top:0,left:0,width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0.5)',zIndex:'999'}}></div>}
    
                            {/* Bootstrap Modal */}
                            <div className="modal mt-5 pt-5" tabIndex="-1" role="dialog" style={{ display: showModal[index] ? 'block' : 'none' }}>
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                <div className='d-flex justify-content-end'><IoIosCloseCircle size={40} color='#F04A00'
                                    onClick={() => handleCloseModal(index)}
                                    style={{cursor:'pointer'}}
                                /></div>
                                <div className="mt-4">
                                    <h4 className="text-center">Do you want to delete this<br/> enlistee file?</h4>
                                </div>
                                <div className=" mt-3  px-5 mb-3">
                                    <div className='d-flex justify-content-center'>
                                    <div className='p-2 d-flex justify-content-center' style={{width:'100%'}}>
                                    <button
                                        type="button"
                                        className="btn px-5 py-2"
                                        onClick={() => {
                                            console.log("from modal ",item);
                                            handleDelete(item.id,index)
                                        }}
                                        style={{ backgroundColor: '#F04A00', color: 'white' }}
                                    >
                                        Confirm
                                    </button>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            </div>
                            {showForm && radioCheckedIndex === index && (
                                <div className='px-4' style={{width:'100%'}}>
                                    <div className='d-flex justify-content-between align-items-center'>
                                        <div className='d-flex flex-column w-40 position-relative' style={{marginLeft:70}}>
                                            <label htmlFor=''>Enlistment Year</label>
                                            <div className=''>
                                                <Select
                                                    options={options}
                                                    styles={customStyles}
                                                    value={selectedOption}
                                                    onChange={handleSelectChange}
                                                    placeholder='Select Enlistment Year'
                                                />
                                            </div>
                                        </div>
                                        <div className=' mt-3'>
                                            <button className='btn px-5 text-center' style={{ backgroundColor: '#F04A00', color: 'white' }}
                                                onClick={() => handleAllocationStart(index)}
                                            >
                                                Allocate
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    ))
                ):(
                    <div className='d-flex justify-content-center align-items-center' style={{height:'300px'}}>
                        <div className='d-flex justify-content-center align-items-center'>
                            <div className='text-center'>
                                <ImDropbox size={50} color='#F04A00'/>
                                <p className='mt-3' style={{fontWeight:'bold'}}>Upload File to Allocate!</p>
                            </div>
                        </div>
                    </div>
                )
            }
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />

            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
                />
                {/* Same as */}
            <ToastContainer />
        </div>
    );
};

export default SelectFile;