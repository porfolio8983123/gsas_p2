import React, { useState,useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { CSVLink } from 'react-csv';
import { updateSideLine } from '../../store';
import { useDispatch } from 'react-redux';
import AllocationTable from '../tables/AllocationTable';

export default function Output({setNextStep,setPreviousStep,allStudentData,type}) {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [allStudent,setAllStudent] = useState([]);
  const [downloadStudent,setDownloadStudent] = useState([]);

  useEffect(() => {
    const allData = allStudentData.map(school => ({
      academy: school,
      students: school.students.map(student => student)
    }));

    setAllStudent(allData);
  }, [allStudentData]);

  useEffect(() => {
    console.log('from output ',allStudent);
  },[allStudent])

  useEffect(() => {
    const allData = [];

    allStudentData.forEach(school => {
      school.students.forEach(student => {
        const studentObject = {
          CID: student.CID,
          name: student.name,
          gender: student.gender,
          dzongkhag: student.dzongkhag,
          gewog: student.gewog,
          skilling: school.abbrebation,
        };

        allData.push(studentObject);
      });
    });

    setDownloadStudent(allData);
  }, [allStudentData]);
  
  return (
    <div>
        <div className='d-flex justify-content-end me-4'>
          <div
            className='mb-2 d-flex justify-content-end mt-5'
          ><CSVLink className='btn px-4 arrow-btn border-danger me-5' data = {downloadStudent} filename={'allAcademyStudent.csv'}>Export All</CSVLink></div>
          <div
            className='mb-2 d-flex justify-content-end mt-5'
          ><button className='btn px-4 arrow-btn border-danger me-5' 
            onClick={() => {
              console.log("clicking button")
              navigate('/Gyalsung');
              dispatch(updateSideLine(0))
            }}
          >Complete</button></div>
        </div>
        <div style={{maxHeight: 'calc(100vh - 300px)',
          overflowY: 'scroll',marginBottom:'500px'}}>
          {allStudentData && allStudentData.length > 0 &&
            allStudentData.map((item,index) => (
              <div key={index}>
                <h5>{item.abbrebation}</h5>
                <AllocationTable studentData={item.students} type = {type}/>
              </div>
            ))
          }
        </div>
    </div>
  )
}
