import React, { useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PreviewTable from '../tables/PreviewTable';
import { useSelector, useDispatch } from 'react-redux';
import { addStudentData, updateData } from '../../store';
import EnlisteeUploadModal from '../modals/EnlisteeUploadModal';
import { useAddStudentMutation } from '../../store';

function Preview({ setPreviousStep, setNextStep }) {

  const dispatch = useDispatch();

  const {name,year,fileContent,studentData} = useSelector((state) => state.file);

  const [addStudent, {isLoading, isError, data}] = useAddStudentMutation();

  const [csvData,setCsvData] = useState([]);
  const [currentTotalMale,setCurrentTotalMale] = useState(0);
  const [currentTotalFemale,setCurrentTotalFemale] = useState(0);
  const [dataUploading,setDataUploading] = useState(false);

  useEffect(() => {
    const fileData = fileContent;

    const lines = fileData.split('\n');
    const headers = lines[0].split(',').map(header => header.replace(/\r/g, ''));

    const parsedData = [];

    for (let i = 1; i < lines.length; i++) {
        const values = lines[i].split(',');
        if (values.length === headers.length) {
            const rowData = {};
            headers.forEach((header,index) => {
                rowData[header.trim()] = values[index].replace(/\r/g, '');
            })
            parsedData.push(rowData);
        }
    }

    setCsvData(parsedData);
    dispatch(addStudentData(parsedData));
    },[fileContent]);

    useEffect(() => {
      if (csvData) {
        const genderCounts = csvData.reduce((counts, row) => {
          const gender = row['SEX']; 
          counts[gender] = (counts[gender] || 0) + 1;
          return counts;
        }, {});
  
        setCurrentTotalMale(genderCounts.M);
        setCurrentTotalFemale(genderCounts.F);
      }
    }, [csvData]);

    const cancelDataStoringProcess = () => {
      setDataUploading(false);
    }

    const handleStudentDataSubmit = () => {
      setDataUploading(true);

      const data = {
        studentData: studentData
      }

      addStudent(data)
      .unwrap()
      .then((response) => {
        console.log("response from enlistee uploading ",response)
        setDataUploading(false);
        setNextStep();
        
      })
      .catch((error) => {
        console.log("error ",error);
        setDataUploading(false);
      })
    }

  return (
    <div style={{
      maxHeight: 'calc(100vh - 100px)',
      overflowY: 'scroll',
      scrollbarWidth: 'thin',
      paddingBottom:'50px'
    }}>
      <div className=' shadow pb-4'>
      <div className='mt-5'>
          <div className=" " >
            <div action="" style={{ backgroundColor: '#02344F',padding:'20px',borderRadius:'10px',color:'white' }}>
              <div className=' mt-2 d-flex justify-content-between'>
                <h6 >File Name: <span  style={{color:"#F04A00"}}>{name}</span></h6>
                <h6>Total Enlistee: <span style={{color:"#F04A00"}}>{currentTotalFemale + currentTotalMale}</span></h6>
                <h6>Total Male: <span style={{color:"#F04A00"}}>{currentTotalMale}</span></h6>
                <h6>Total Female: <span style={{color:"#F04A00"}}>{currentTotalFemale}</span></h6>
              </div>
            </div>
          </div>
        </div>
        <div className=''>
          <div>
            {csvData?.length > 0 && 
              <PreviewTable students = {csvData}/>
            }
          </div>
          <div className='d-flex justify-content-between mt-5' style={{marginBottom:20}}>
           <button onClick={setPreviousStep} className='btn' style={{backgroundColor:'#F04A00',color:"white"}} disabled={dataUploading}>Previous</button>
        <div>
        <button className="btn" style={{ backgroundColor: '#F04A00', color: 'white' }}
          onClick={handleStudentDataSubmit}
          disabled={dataUploading}
        >
          Next
        </button>
        </div>  
      </div>
    </div>
     
        
      </div>
      <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
      />

      <EnlisteeUploadModal openModal={dataUploading} toggleModal={cancelDataStoringProcess}/>

    </div>
    
  );
}

export default Preview;
