import React, { useState,useEffect } from 'react'
import { useSelector } from 'react-redux';
import { useFetchAcademySkillingQuery,useStartAllocationMutation, useFetchAllocatedQuery } from '../../store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Configuration } from '../configurationComponents/Configuration';
import { RunModal } from '../RunModal';
import HSS from '../criteria/HSS';


function Setting({setNextStep,setPreviousStep,setAllocation,setName,setCurrentAllocations,currentAllocations}) {

  console.log("current allocation from setting ",currentAllocations);

  const {data:academySkilling,isLoading:academySkillingLoading,isError:academySkillingError,refetch:refetchAcademySkilling} = useFetchAcademySkillingQuery();

  useEffect(() => {
    console.log("data of academy skilling ",academySkilling);
  },[academySkilling])

  useEffect(()=>{
    refetchAcademySkilling();
  },[])

  const [showModal, setShowModal] = useState(false);
  const [allocationName,setAllocationName] = useState('');
  const [allocationId,setAllocationId] = useState(null);
  const [academyIndex,setAcademyIndex] = useState(0);

  const fileData = useSelector((state) => state.file);

  useEffect(() => {
    console.log("current academy ",academySkilling?.data[academyIndex]?.skillingName);
  },[academyIndex])

  const settingShowModal = (boolean) => {
    setShowModal(boolean);
  }
  
  const { data: fetchedData,refetch } = useFetchAllocatedQuery(allocationId);

  useEffect(() => {
    if (allocationId) {
      refetch();
    }
    if (allocationId && fetchedData) {
      // Check if the fetched data with the same ID already exists in the state
      const isDuplicate = currentAllocations?.some((item) => item.id === fetchedData?.data.id);
  
      if (!isDuplicate) {
        // Update state by appending new data to the existing array
        setCurrentAllocations((prevAllocations) => [...prevAllocations, fetchedData?.data]);
      }
    }
  
  }, [allocationId,fetchedData]);

  console.log("from setting ", fileData);

  if (academySkillingLoading) {
    return (
      <div>
        Loading
      </div>
    )
  }

  const handleSubmit2 = () => {
    // Add your submit logic here
    setShowModal(true);
  };

  const handleCloseModal = (boolean) => {
    setShowModal(false);
  };

  const settingAllocationName = (name) => {
    setAllocationName(name);
  }

  const settingAcademyIndex = () => {
    setAcademyIndex(academyIndex + 1);
    setShowModal(false)
  }

  const resetAcademyIndex = () => {
    setAcademyIndex(0);
    setShowModal(false);
  }

  console.log(allocationName);

  const formatDate = (dateString) => {
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
    };
    return new Date(dateString).toLocaleString('en-US', options);
  };



  return (
    <div style={{maxHeight: 'calc(100vh - 100px)',
    overflowY: 'scroll',}}>
       <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
      />
        <div className='shadow-lg p-3 mb-2 bg-white rounded'>
          <div className='mb-5' style={{width:'100%'}}>
              <div className='d-flex justify-content-between rounded border border-dark py-3 px-3' style={{width:'100%'}}>
                {academySkilling?.data?.map((academyskill,index) => (
                  <div key={index} className='px-3' style={{cursor:'pointer',borderBottom:academyIndex === index?'4px solid red':''}}><p>{academyskill.name}({academyskill.skillingName})</p></div>
                ))

                }
              </div>
            </div>
          
          <div>
            {academySkilling?.data[academyIndex].skillingName === "ICT" || academySkilling?.data[academyIndex].skillingName === "CSS" ? (
              <Configuration academySkillingData = {academySkilling?.data[academyIndex]}/>
            ):(
              <div>
                <HSS academySkillingData = {academySkilling?.data[academyIndex]}/>
              </div>
            )

            }
          </div>
          
          <div className='d-flex justify-content-center mt-5' style={{marginBottom:20}}>
            {academyIndex === 0 ? (
              <button onClick={setPreviousStep} className='btn px-4' style={{backgroundColor:'#F04A00',color:"white"}}>Previous</button>
            ):(
              <button onClick={() => {
                setAcademyIndex(academyIndex - 1);
              }} className='btn px-4' style={{backgroundColor:'#F04A00',color:"white"}}>Previous</button>
            )

            }
            <div>
          <div>
            {academySkilling?.data?.length === academyIndex ? (
              <button className="btn ms-5 px-5" onClick={() => {
                setNextStep();
                }} style={{ backgroundColor: '#F04A00', color: 'white' }}>
                Run
              </button>
            ):(
              <button className="btn ms-5 px-5" onClick={() => {
                setShowModal(true);
                }} style={{ backgroundColor: '#F04A00', color: 'white' }}>
                Run
              </button>
            )
              
            }
          </div>
    </div>
          </div>

          <RunModal setAllocationId = {setAllocationId} resetAcademyIndex = {resetAcademyIndex} setNextStep = {setNextStep} showModal={showModal} settingShowModal = {settingShowModal} handleCloseModal = {handleCloseModal} allocationName = {allocationName} settingAllocationName = {settingAllocationName} settingAcademyIndex = {settingAcademyIndex} academyIndex = {academyIndex} academySkillingData = {academySkilling?.data[academyIndex]} academySkillingLength = {academySkilling?.data?.length}/>
        </div>
        <div className='mt-3' style={{marginBottom:'200px'}}>
          <h5 className='mb-4' style={{fontWeight:'bold'}}>Allocation Run List</h5>
          <div> 
          {currentAllocations && currentAllocations.length > 0 && 
            currentAllocations.slice().reverse().map((item,index) => (
              <div key={index} className='container-fluid d-flex justify-content-center  align-items-center mt-3' onClick={() => {
                console.log(item.allocationId)
                setAllocation(item.allocationId)
                setName(item.nameOfAllocation)
                setNextStep()
              }}
              style={{cursor:'pointer'}}
              >
                <div className='ps-2 pe-3 bg-body rounded d-flex justify-content-between  align-items-center w-100 border border-dark '>
                    <div className='d-flex  align-items-center mt-3'>
                        <div><p className='ms-3'>{item.nameOfAllocation}</p></div>
                        <div><p className='' style={{marginLeft:'200px'}}>{formatDate(item.created_at)}</p></div>
                    </div>
                    <div className='' style={{marginRight:'50px'}}>
                      <h6>{item.allocationId}</h6>
                    </div>
                </div>
              </div>
            ))
          }
          </div>
        </div>
    </div>
  )
}

export default Setting
