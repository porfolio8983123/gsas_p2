import React, { useState,useEffect } from 'react';
import {Outlet, useNavigate} from 'react-router-dom';
import {
    FaBars,
    FaTh,
    FaFileUpload,
    FaPlus,
} from 'react-icons/fa';
import { FaLocationDot } from 'react-icons/fa6';
import { MdAccountCircle } from 'react-icons/md';
import { IoLogOutOutline } from 'react-icons/io5';
import { AiOutlineFileDone } from 'react-icons/ai';
import logo from '../assets/img/desung logo 1.png';
import { updateSideLine } from '../store';
import { useDispatch,useSelector } from 'react-redux';
import Cookies from 'js-cookie';

const menuItem = [
    {
      path: '/Gyalsung',
      name: 'Dashboard',
      icon: <FaTh />
    },
    {
      path: 'fileupload',
      name: 'File upload',
      icon: <FaFileUpload />
    },
    {
      path: 'allocation',
      name: 'Allocation',
      icon: <FaPlus />
    },
    {
      path: 'academy',
      name: 'Academy',
      icon: <FaLocationDot />
    },
    {
      path: 'allocationrun',
      name: 'Allocation run',
      icon: <AiOutlineFileDone />
    },
    {
      path: 'report',
      name: 'Report',
      icon: <FaBars />
    },
    {
      path: 'profile',
      name: 'Profile',
      icon: <MdAccountCircle />
    }
  ];

const Sidebar = ({handleLogout}) => {

  const navigate = useNavigate();

  const [token,setToken] = useState('');

    const dispatch = useDispatch();
    const slideLineValue = useSelector((state) => state.sideLine)

    console.log(slideLineValue)

    useEffect(() => {
      const token = Cookies.get('auth_token');
      setToken(token);
    }, [])

    const logout = async () => {
      await fetch('http://localhost:8000/api/logout', {
          method: 'POST',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
          }
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              console.log("Failed")
            }
          })
          .then((data) => {
            Cookies.remove('auth_token');
            handleLogout();
            console.log(data);
          })
          .catch((error) => {
            console.log(error.message);
          })
    }

    const handleMenuItemClick = async (item, index) => {
      dispatch(updateSideLine(index));
  
      if (item.path === 'logout') {
        await fetch('http://localhost:8000/api/logout', {
          method: 'POST',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
          }
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              console.log("Failed")
            }
          })
          .then((data) => {
            Cookies.remove('auth_token');
            handleLogout();
            console.log(data);
          })
          .catch((error) => {
            console.log(error.message);
          })
      } else {
        navigate(item.path);
      }
    }

  return (
    <div className='d-flex' style={{height:"100vh"}}>
        <div className='d-flex flex-column justify-content-between align-items-center' style={{minWidth:'250px',backgroundColor:"#02344F"}}>
            <div className='d-flex flex-column text-white justify-content-center align-items-center'>
                <img src={logo} alt="" />
                <div className=''>
                    <p className='text-center'>Gyalsung Smart Allocation <br /> System</p>
                    <hr style={{color:'white',height:"2px"}}/>
                </div>
            </div>

           <div className=''>
            {
                    menuItem.map((item, index) => (
                    <div
                        key={index}
                        className='text-white d-flex justify-content-center py-3'
                        onClick={() => handleMenuItemClick(item, index)}
                        style={{ cursor: 'pointer' }}
                    >
                    <div className='d-flex justify-content-start' style={{width:"160px", color: slideLineValue.value === index ? "#F04A00":"white" }}
                        onClick={() => {
                            dispatch(updateSideLine(index));
                        }}
                    >
                        <div className='me-4'
                        >{item.icon}</div>
                        <div className=''
                        >{item.name}</div>
                    </div>
                    </div>
                    ))
                }
           </div>

           <div
                className='text-white d-flex justify-content-center py-2'
                style={{ cursor: 'pointer' }}
                >
                <div className='d-flex justify-content-start' style={{width:"160px"}}
                  onClick={() => {
                    logout();
                  }}
                >
                    <div className='me-4'><IoLogOutOutline size={21} /></div>
                    <div className=''>Logout</div>
                </div>
                </div>
            </div>
        <main className='border px-3' style={{width:'100%',overflow:"hidden"}}>
            <Outlet />
        </main>
    </div>
  )
}

export default Sidebar