import React, { useState, useRef } from "react";
import Header from "./../components/Header";
import { BsPlus, BsArrowLeft, BsArrowRight } from "react-icons/bs"; // Importing the plus icon from Bootstrap Icons
import academyImage from "../assets/img/academy2.png";
import "../style/Academy.css";
import Switch from "react-switch"; // Import the Switch component
import { FaMapMarkerAlt } from "react-icons/fa";
import { useUpdateAcademyStatusMutation } from "../store";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useEffect } from "react";
import gyalpozhinglogo from "../assets/img/logos/gyalpozhing.png";
import Modal from "react-modal"; // Import the Modal component
import { useFetchSkillingsQuery, useAddSkillingMutation,useDeleteSkillingMutation,useUpdateSkillingMutation } from "../store";

Modal.setAppElement('#root');

const initialAcademies = [
  {
    name: "Gyalpozhing Academy",
    location: "Mongar",
    capacity: "1000",
    image: academyImage,
    isActive: false,
  },
  {
    name: "Khotokha Academy",
    location: "Wangdue Phodrang",
    capacity: "1500",
    image: academyImage,
    isActive: false,
  },
  {
    name: "Jamtsholing Academy",
    location: "Samtse",
    capacity: "1200",
    image: academyImage,
    isActive: false,
  },
  {
    name: "Pemathang Academy",
    location: "Samdrup Jongkhar",
    capacity: "1800",
    image: academyImage,
    isActive: false,
  },
];

let skillingOptions = [
  {
    value: "ICT",
    label: "Information & Communication Technology Security Skilling",
  },
  { value: "CSS", label: "Community Security Skilling (CSS)" },
  { value: "HSS", label: "Home Security Skilling (HSS)" },
  { value: "FSS", label: "Food Security Skilling (FSS)" },
];

export default function AllAcademy({
  setAddAcademy,
  setEditAcademy,
  data,
  getAcademy,
}) {

  const [academyId,setAcademyId] = useState();
  const [skillingId,setSkilllingId] = useState();

  const [activeAcademyIndex, setActiveAcademyIndex] = useState(0);
  const [academies, setAcademies] = useState(data);

  const [isEdit,setIsEdit] = useState(true);

  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [capacity, setCapacity] = useState("");
  const [inputValue, setInputValue] = useState("");
  const [selectedSkilling, setSelectedSkilling] = useState("");
  const [gyalpozhingLogo, setGyalpozhingLogo] = useState(gyalpozhinglogo); // Define gyalpozhingLogo state
  const fileInputRef = useRef(null);

  const [updateAcademyStatus,{isLoading,isError}] = useUpdateAcademyStatusMutation();
  const {data:skillings,isLoading:skillingLoading,isError:isSkillingError} = useFetchSkillingsQuery();

  const [updateSkilling] = useUpdateSkillingMutation();
  const [addSkilling] = useAddSkillingMutation();
  const [deleteSkilling] = useDeleteSkillingMutation();

  useEffect(() => {
    console.log("skilling data ",skillings);
    let allSkillings = [];
    skillings?.data?.forEach(element => {
      console.log(element.skillingName);
      allSkillings.push({value:element.skillingName,label:element.abbrebation});
    });
    skillingOptions = allSkillings;
  },[skillings]);

  useEffect(() => {
    setAcademies(data);
    console.log("data from all academy ",data);
  },[data])

  const toggleAcademyStatus = (index,status) => {
    console.log("id ",index,status)
    let academyStatus;
    if (status === 1) {
      academyStatus = false;
    } else {
      academyStatus = true;
    }
    const data = {status:academyStatus}

    console.log("data ",data);

    updateAcademyStatus({id:index,data})
    .unwrap()
    .then((response) => {
      console.log(response);
      getAcademy();
    })
    .catch((error) => {
      console.log(error)
    })
  };

  const goToPreviousPage = () => {
    setActiveAcademyIndex((prevIndex) =>
      prevIndex > 0 ? prevIndex - 1 : initialAcademies.length - 1
    );
  };

  const goToNextPage = () => {
    setActiveAcademyIndex((prevIndex) =>
      prevIndex < initialAcademies.length - 1 ? prevIndex + 1 : 0
    );
  };

  const currentAcademy = initialAcademies[activeAcademyIndex];

  if (!currentAcademy) {
    return null; // or display a loading message or some fallback UI
  }

  const openModal = () => {
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };

  const handleCapacityChange = (e) => {
    setCapacity(e.target.value);
  };

  const handleDeleteService = (academySkillingId) => {
    deleteSkilling({academySkillingId})
    .unwrap()
    .then((respones) => {
      console.log(respones);
      getAcademy();
    })
    .catch((error) => {
      console.log(error);
    })
  }

  const handleSubmit = () => {
    // Handle form submission here
    if (isEdit) {
      console.log("editing skilling",selectedSkilling);
      console.log("skillling id ",capacity);
      const data = {
        skilling:selectedSkilling,
        intakeSize:capacity
      }

      updateSkilling({academySkillingId:skillingId,data})
      .unwrap()
      .then((response) => {
        console.log(response);
        getAcademy();
        closeModal()
      })
      .catch((error) => {
        console.log(error)
        console.log("hell",error.data.error)
        toast.error(error.data.error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          });
      })

    } else {
      console.log("adding skilling");
      console.log("academy id ",academyId);
      const data = {
        academyId:academyId,
        skilling:selectedSkilling,
        intakeSize:capacity
      }

      addSkilling({data})
      .unwrap()
      .then((response) => {
        console.log(response);
        getAcademy();
        closeModal();
      })
      .catch((error) => {
        console.log(error)
        toast.error(error.data.error, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          });
      })
    }
  };

  const handleSkillingChange = (e) => {
    setSelectedSkilling(e.target.value);
  };

  const getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  return (
    <div className="">
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <div
        className="bg-white d-flex justify-content-between align-items-center px-4"
        style={{ zIndex: 999,marginTop:'10px'}}
      >
        <h3 className="m-0">Academy</h3>
        <div className="d-flex align-items-center">
          <h6 className="mb-0 me-4">Add Academy</h6>
          <div className="p-2 me-4 rounded">
            <button
              className="p-3 rounded h3"
              style={{
                borderColor: "#F04A00",
                borderWidth: "3px",
                borderStyle: "solid",
                background: "white",
                color: "#F04A00",
              }}
              onClick={() => setAddAcademy()}
            >
              <BsPlus />
            </button>
          </div>
        </div>
      </div>

      <div
        style={{
          maxHeight: 'calc(100vh - 100px)',
          overflowY: 'scroll',
          scrollbarWidth: 'thin',
          paddingBottom:'50px'
        }}
      >
        {academies?.map((academy, index) => (
          <div className="px-4" key={index}
          
          >
            <div className="p-3 mt-4 shadow p-3 mb-5 rounded">
              <div
                className="image-container"
                style={{
                  position: "relative",
                  overflow: "hidden",
                  height: "400px",
                  backgroundImage: `url(${academyImage})`,
                  backgroundSize: "cover",
                  backgroundPosition: "center",
                }}
              >
                <div
                  className="switch-container"
                  style={{
                    position: "absolute",
                    top: "20px",
                    right: "20px",
                    display: "flex",
                    alignItems: "center",
                  }}
                >
                  <label
                    htmlFor="camp-status"
                    className="switch-label me-2 text-white"
                  >
                    Academy Status
                  </label>
                  <Switch
                    id='camp-status'
                    onChange={() => toggleAcademyStatus(academy.academyId,academy.status)}
                    checked={academy.status === 0?false:true}
                    uncheckedIcon={false}
                    checkedIcon={false}
                    onColor='#F04A00'
                    offColor='#cccccc'
                    width={50}
                />
                </div>
                <div
                  className="text-container"
                  style={{
                    position: "absolute",
                    top: "20%",
                    left: "10%",
                    color: "#fff",
                    textAlign: "left",
                    zIndex: 2,
                  }}
                >
                  <h3>{academy.name}</h3>
                  <div className="d-flex mt-3">
                    <FaMapMarkerAlt className="map-marker-icon mt-1" />
                    <p className="ms-2">{academy.dzongkhag}</p>
                  </div>
                  <p>Capacity: {academy.capacity}</p>
                  <div className="mt-4">
                    <button
                      className="btn px-5"
                      style={{ backgroundColor: "#F04A00", color: "white" }}
                      onClick={() => setEditAcademy(academy)}
                    >
                      Edit
                    </button>
                  </div>
                </div>
                <div className="image-overlay"></div>
              </div>
              <div className="mt-3">
                <h5 className="">Skilling</h5>
                {academy.skills.length > 0 ? (
                  <div className="">
                    {academy.skills.map((skill,index) => (
                      <div
                        className="d-flex justify-content-between mb-3"
                        style={{ height: "70px", backgroundColor: "#D9D9D9" }}
                        key={index}
                      >
                        <div className="d-flex align-items-center">
                          <div
                            className=""
                            style={{
                              width: "20px",
                              height: "100%",
                              backgroundColor: getRandomColor(),
                            }}
                          ></div>
                          <div className="ms-3">
                            <h6>
                              {skill.skillingName}{" "}
                              <span style={{ fontWeight: "normal" }}>
                                ({skill.abbrebation})
                              </span>
                            </h6>
                            <h6>Intake size: {skill.intakeSize}</h6>
                          </div>
                        </div>
                        <div
                          className="d-flex align-items-center"
                          style={{ marginRight: "50px" }}
                        >
                          <div>
                            <button
                              className="ps-4 pe-4 py-1 me-2"
                              style={{
                                borderRadius: "8px",
                                backgroundColor: "#F04A00",
                                color: "white",
                                borderWidth: "0",
                              }}
                              onClick={() => {
                                openModal();
                                setSelectedSkilling(skill.skillingName);
                                setCapacity(skill.intakeSize);
                                setIsEdit(true);
                                setSkilllingId(skill.academySkillingId);
                              }}
                            >
                              Edit
                            </button>
                            <button
                              className="ps-3 pe-3 py-1"
                              style={{
                                borderRadius: "8px",
                                color: "black",
                                borderColor: "#F04A00",
                                background:'white'
                              }}
                              onClick={() => {
                                handleDeleteService(skill.academySkillingId);
                              }}

                              onMouseOver={(event) => {
                                  event.target.style.backgroundColor = '#F04A00';
                              }}
                              onMouseOut={(event) => {
                                  event.target.style.backgroundColor = 'white';
                              }}
                            >
                              Delete
                          </button>
                          </div>
                        </div>
                      </div>
                    ))

                    }
                  </div>
                ):(
                  <div>
                    Not Skill Yet!
                  </div>
                )

                }
                <div
                  style={{ width: "95.1%" }}
                  className="d-flex justify-content-between"
                >
                  <img
                    className="img-fluid"
                    src={gyalpozhinglogo}
                    alt=""
                    style={{ width: "100px" }}
                  />
                  <div className="d-flex align-items-center">
                    <button
                      className="px-3 py-1"
                      style={{
                        borderRadius: "8px",
                        borderColor: "#F04A00",
                        color: "black",
                        backgroundColor: "white",
                      }}
                      onClick={() => {
                        openModal();
                        setIsEdit(false);
                        setAcademyId(academy.academyId);
                      }}
                    >
                      Add Skilling
                    </button>

                    {/* Modal for adding skilling */}
                    <Modal
                      isOpen={modalIsOpen}
                      onRequestClose={closeModal}
                      style={{
                        overlay: {
                          backgroundColor: "rgba(0, 0, 0, 0.35)",
                          zIndex: 1000,
                        },
                        content: {
                          position: "absolute",
                          top: "50px",
                          left: "50%",
                          transform: "translateX(-50%)",
                          width: "500px",
                          height:'500px',
                          padding: "40px",
                          borderRadius: "8px 8px 0 0",
                          borderTop: "15px solid #F04A00",
                          boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.2)",
                          backgroundColor: "white",
                          zIndex: "1000",
                        },
                      }}
                    >
                      <div style={{ textAlign: "center" }}>
                        <input
                          type="file"
                          accept="image/*"
                          onChange={(e) => {
                            const file = e.target.files[0];
                            if (file) {
                              const reader = new FileReader();
                              reader.onloadend = () => {
                                // Update the image source with the uploaded file
                                setGyalpozhingLogo(reader.result);
                              };
                              reader.readAsDataURL(file);
                            }
                          }}
                          style={{ display: "none" }}
                          ref={fileInputRef}
                        />
                        <img
                          className="img-fluid"
                          src={gyalpozhingLogo} // Use the state variable to display the image
                          alt=""
                          style={{ width: "200px", cursor: "pointer" }}
                          onClick={() => fileInputRef.current.click()} // Trigger file input click on image click
                        />
                      </div>

                      <div>
                        <p
                          style={{
                            fontWeight: "600",
                            fontFamily: "Montserrat, sans-serif",
                          }}
                        >
                          Skilling Name
                        </p>
                        <select
                          value={selectedSkilling}
                          onChange={handleSkillingChange}
                          style={{
                            marginBottom: "10px",
                            height: "40px",
                            fontSize: "16px",
                            padding: "8px 12px",
                            borderRadius: "4px",
                            border: "2px solid #ccc",
                            width: "100%",
                          }}
                        >
                          {skillingOptions.map((option) => (
                            <option
                              key={option.value}
                              value={option.value}
                              disabled={option.value === ""}
                            >
                              {option.label}
                            </option>
                          ))}
                        </select>

                        <p
                          style={{
                            fontWeight: "600",
                            fontFamily: "Montserrat, sans-serif",
                          }}
                        >
                          Intake size
                        </p>
                        <input
                          type="text"
                          value={capacity}
                          onChange={handleCapacityChange}
                          placeholder="Enter Intake Capacity..."
                          style={{
                            marginBottom: "10px",
                            height: "40px",
                            fontSize: "16px",
                            padding: "8px 12px",
                            borderRadius: "4px",
                            border: "2px solid #ccc",
                            width: "100%",
                          }}
                        />
                      </div>

                      <div style={{ marginTop: "30px" }}>
                        <button
                          onClick={closeModal}
                          style={{
                            marginRight: "50px",
                            padding: "8px 50px",
                            marginLeft: "40px",
                            backgroundColor: "white",
                            color: "#F04A00",
                            borderRadius: "8px",
                            border: "2px solid #F04A00",
                          }}
                        >
                          Cancel
                        </button>

                        <button
                          onClick={handleSubmit}
                          style={{
                            padding: "8px 50px",
                            backgroundColor: "#F04A00",
                            color: "white",
                            borderRadius: "8px",
                            border: "2px solid #F04A00",
                          }}
                        >
                          Confirm
                        </button>
                      </div>
                    </Modal>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
