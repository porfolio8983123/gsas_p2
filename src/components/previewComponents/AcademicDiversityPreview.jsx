import React, { useEffect, useState } from 'react';
import { PieChart, pieArcLabelClasses } from '@mui/x-charts/PieChart';
import Slider from '@mui/material/Slider';

const initialData = [
  { id: 0, value: 60, label: 'Academic Mark' },
  { id: 1, value: 40, label: 'Diversity' },
];

export default function AcademicDiversityPreview({diversity}) {

  const [data, setData] = useState(initialData);

  useEffect(() => {
    const data = [
      { id: 0, value: 100 - diversity, label: 'Academic Mark' },
      { id: 1, value: diversity, label: 'Diversity' },
    ];

    setData(data);
  },[])

  const totalValue = data.reduce((acc, curr) => acc + curr.value, 0);

  let dataPercentages;
  if (totalValue === 0) {
    dataPercentages = data.map(item => ({
      ...item,
      value: 50,
      color: item.label === 'Diversity' ? '#F04A00' : '#02344F',
    }));
  } else {
    dataPercentages = data.map((item, index) => ({
      ...item,
      value: (item.value / totalValue) * 100,
      color: index === 0 ? '#F04A00' : '#02344F',
    }));
  }

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <PieChart
          margin={{ left: 100 }}
          series={[
            {
              innerRadius: 30,
              arcLabel: (item) => `${item.value}%`,
              arcLabelMinAngle: 45,
              data: dataPercentages,
              highlightScope: { faded: 'global', highlighted: 'item' },
              faded: { innerRadius: 30, additionalRadius: -30, color: 'gray' },
            },
          ]}
          slotProps={{
            legend: {
              hidden: true // Hide legend
            }
          }}
          sx={{
            [`& .${pieArcLabelClasses.root}`]: {
              fill: 'white',
              fontWeight: 'bold',
            },
          }}
          height={160}
          className="custom-pie-chart"
        />
      </div>
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
        <div style={{ marginRight: '20px', display: 'flex', alignItems: 'center' }}>
          <div style={{ width: '20px', height: '20px', backgroundColor: '#F04A00', marginRight: '5px',borderRadius:'20px' }}></div>
          <span>Academic Mark</span>
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ width: '20px', height: '20px', backgroundColor: '#02344F', marginRight: '5px',borderRadius:'20px' }}></div>
          <span>Diversity</span>
        </div>
      </div>
    </div>
  );
}
