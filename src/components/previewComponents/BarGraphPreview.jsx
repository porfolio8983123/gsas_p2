import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Label, ResponsiveContainer } from 'recharts';

export default function BarGraphPreview({dzongkhagCount}) {
  const data = [
    { name: 'Thim', value1: 4 },
    { name: 'Paro', value1: 3 },
    { name: 'Haa', value1: 2 },
    { name: 'Tgang', value1: 4 },
    { name: 'Tyang', value1: 3 },
    { name: 'Sjong', value1: 2 },
    { name: 'Chuk', value1: 4 },
    { name: 'Trong', value1: 3 },
    { name: 'Wang', value1: 2 },
    { name: 'Puna', value1: 4 },
    { name: 'Gasa', value1: 3 },
    { name: 'Bumt', value1: 2 },
    { name: 'Mong', value1: 4 },
    { name: 'Daga', value1: 3 },
    { name: 'Tsir', value1: 2 },
    { name: 'Sarp', value1: 4 },
    { name: 'Zhem', value1: 3 },
    { name: 'Pemag', value1: 2 },
    { name: 'Lhue', value1: 4 },
    { name: 'Sam', value1: 3 },
  ];

  console.log("dzongkhag count ",dzongkhagCount);

  return (
    <div style={{ width: '100%', height: 400 }}> {/* Set a fixed height for the container */}
      <ResponsiveContainer width="100%" height="100%">
        <BarChart data={dzongkhagCount}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" angle={-90} textAnchor="end" interval={0} tick={{ dy: 10 }} height={60} />
          <YAxis>
            <Label value="No of Enlistee" angle={-90} position="insideLeft" />
          </YAxis>
          <Tooltip />
          <Bar
            dataKey="TotalEnlistee"
            fill="rgba(240,74,0,0.8)"
            barSize={30}
          />
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
}
