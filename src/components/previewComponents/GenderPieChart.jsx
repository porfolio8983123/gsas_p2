import React, { useEffect, useState } from 'react';
import { PieChart, pieArcLabelClasses } from '@mui/x-charts/PieChart';
import Slider from '@mui/material/Slider';

const initialData = [
  { id: 0, value: 800, label: 'Male' },
  { id: 1, value: 1200, label: 'Female' },
];

export default function GenderPieChart({totalStudent,maleCount, femaleCount}) {

  const capacity = totalStudent; // Total capacity

  const [data, setData] = useState(initialData);

  const totalValue = data.reduce((acc, curr) => acc + curr.value, 0);

  useEffect(() => {

    console.log("male count from gender ",maleCount);
    console.log('female count from gender ',femaleCount)
    const data = [
      { id: 0, value: Math.floor(parseInt(maleCount)), label: 'Male' },
      { id: 1, value: Math.floor(parseInt(femaleCount)), label: 'Female' },
    ];

    setData(data);
  },[])

  let dataPercentages;
  if (totalValue === 0) {
    dataPercentages = data.map(item => ({
      ...item,
      value: 50,
      color: item.label === 'Diversity' ? '#7D7D7D' : 'rgba(45,156,219,0.15)',
    }));
  } else {
    dataPercentages = data.map((item, index) => ({
      ...item,
      value: (item.value / totalValue) * capacity,
      color: index === 0 ? '#7D7D7D' : 'rgba(45,156,219,0.15)',
    }));
  }

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <PieChart
          margin={{ left: 100 }}
          series={[
            {
              innerRadius: 30,
              arcLabel: (item) => `${Math.ceil(item.value)}`,
              arcLabelMinAngle: 45,
              data: dataPercentages,
              highlightScope: { faded: 'global', highlighted: 'item' },
              faded: { innerRadius: 30, additionalRadius: -30, color: 'gray' },
            },
          ]}
          slotProps={{
            legend: {
              hidden: true // Hide legend
            }
          }}
          sx={{
            [`& .${pieArcLabelClasses.root}`]: {
              fill: 'black',
              fontWeight: 'normal',
            },
          }}
          height={160}
          className="custom-pie-chart"
        >
          <text x="50%" y="50%" textAnchor="middle" dominantBaseline="middle" fontSize="15px">{totalStudent}</text>
        </PieChart>
      </div>
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
        <div style={{ marginRight: '20px', display: 'flex', alignItems: 'center' }}>
          <div style={{ width: '20px', height: '20px', backgroundColor: 'rgba(45,156,219,0.15)', marginRight: '5px',borderRadius:'20px' }}></div>
          <span>Female</span>
        </div>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ width: '20px', height: '20px', backgroundColor: '#7D7D7D', marginRight: '5px',borderRadius:'20px' }}></div>
          <span>male</span>
        </div>
      </div>
    </div>
  );
}
