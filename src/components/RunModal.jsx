import { useEffect, useState } from "react";
import { IoIosCloseCircle } from "react-icons/io";
import { useSelector, useDispatch } from "react-redux";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useStartAllocationMutation, updateAllocationId,updateIntakeId, useCssAllocationMutation,useHssAllocationMutation, useFssAllocationMutation, useIctAllocationMutation} from "../store";

export const RunModal = ({setAllocationId,resetAcademyIndex, academySkillingLength,showModal,settingShowModal,handleCloseModal,allocationName,settingAllocationName, settingAcademyIndex, academyIndex, academySkillingData,setNextStep}) => {

    const dispatch = useDispatch();

    console.log("academyskilling from modal ",academySkillingLength);

    const [currectAllocationName,setCurrectAllocationName] = useState('');

    const [startAllocation, {isLoading,isError}] = useStartAllocationMutation();
    const [cssAllocation, {isLoading:cssLoading, isError:cssError}] = useCssAllocationMutation();
    const [hssAllocation, {isLoading:hssLoading,isError:hssError}] = useHssAllocationMutation();
    const [fssAllocation, {isLoading:fssLoading,isError:fssError}] = useFssAllocationMutation();
    const [ictAllocation, {isLoading:ictLoading,isError:ictError}] = useIctAllocationMutation();

    const configuration = useSelector((state) => state.configuration);
    const fileData = useSelector((state) => state.file);

    const tracker = useSelector((state) => state.tracker);

    useEffect(() => {
        console.log("configuration file ",configuration);
        console.log("file data from run modal ",fileData);
        console.log("tracker data ",tracker);
    },[configuration,fileData,tracker])

    const handleSubmit = () => {

        if (currectAllocationName === '') {
            toast.error('Please give an allocation name', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
                });
            return;
        }

        if (academyIndex === academySkillingLength - 1) {
            if (configuration.skilling === 'CSS') {

                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                cssAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from css ",response);
                    if (response.status === 'success') {
                        resetAcademyIndex();
                        setAllocationId(tracker.allocationId);
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            } else if (configuration.skilling === 'HSS') {
                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                hssAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from hss ",response);
                    if (response.status === 'success') {
                        resetAcademyIndex();
                        setAllocationId(tracker.allocationId);
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            } else if (configuration.skilling === 'ICT') {
                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                ictAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from ict ",response);
                    if (response.status === 'success') {
                        resetAcademyIndex();
                        setAllocationId(tracker.allocationId);
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            } else if (configuration.skilling === 'FSS') {
                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                fssAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from fss ",response);
                    if (response.status === 'success') {
                        resetAcademyIndex();
                        setAllocationId(tracker.allocationId);
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            }
        } else {
            console.log("academy index", academyIndex);

            if (academyIndex === 0) {

                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId
                }

                startAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from modal ",response);
                    if (response.status === 'success') {
                        dispatch(updateAllocationId(response.message.allocationId));
                        dispatch(updateIntakeId(response.message.intakeId));
                        settingAcademyIndex();
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            } else if (configuration.skilling === 'CSS') {

                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                cssAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from css ",response);
                    if (response.status === 'success') {
                        settingAcademyIndex();
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            } else if (configuration.skilling === 'HSS') {
                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                hssAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from hss ",response);
                    if (response.status === 'success') {
                        settingAcademyIndex();
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            } else if (configuration.skilling === 'ICT') {
                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                ictAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from ict ",response);
                    if (response.status === 'success') {
                        settingAcademyIndex();
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            } else if (configuration.skilling === 'FSS') {
                const allocationData = {
                    name:currectAllocationName,
                    year:fileData.year,
                    configuration:configuration,
                    academySkillingId:academySkillingData.academySkillingId,
                    allocationId:tracker.allocationId,
                    intakeId: tracker.intakeId
                }

                fssAllocation(allocationData)
                .unwrap()
                .then((response) => {
                    console.log("response from fss ",response);
                    if (response.status === 'success') {
                        settingAcademyIndex();
                    }
                })
                .catch((error) => {
                    console.log(error);
                })
            }
        }
    }

    return (
        <>
             {showModal && <div className="modal-overlay" onClick={() => {
                handleCloseModal(false)
             }} style={{position:'fixed',top:0,left:0,width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0.5)',zIndex:'999'}}></div>}

            {/* Bootstrap Modal */}
            <div className="modal mt-5 pt-5" tabIndex="-1" role="dialog" style={{ display: showModal ? 'block' : 'none' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className='d-flex justify-content-end'><IoIosCloseCircle size={40} color='#F04A00'
                    onClick={() => settingShowModal(false)}
                    style={{cursor:'pointer'}}
                /></div>
                <div className="mt-4">
                    <h4 className="text-center">Do you want to run this<br/> allocation for {academySkillingData?.name} ?</h4>
                </div>
                {academyIndex === 0 &&
                    <div className="">
                
                        <form action="">
                        <div className='d-flex justify-content-center mt-4 p-3'>
                            <div className='px-2'>
                            <label htmlFor="" className='h4 mt-1'>Name</label>
                            </div>
                            <input type="text" value={currectAllocationName} onChange={(e) => setCurrectAllocationName(e.target.value)} placeholder='Enter allocation name' className='form-control' style={{width:'60%'}}/>
                        </div>
                        </form>
                    </div>
                }
                <div className=" mt-3  px-5 mb-3">
                    <div className='d-flex justify-content-center'>
                    <div className='p-2 d-flex justify-content-center' style={{width:'100%'}}>
                        {isLoading || cssLoading || hssLoading || fssLoading || ictLoading ? (
                            <button className="btn px-5 py-2" disabled>
                                <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                            </button>
                        ):(
                            <button type="button" className="btn px-5 py-2" style={{backgroundColor:'#F04A00',color:"white"}}
                                onClick={() => {
                                    handleSubmit();
                                }}
                            >
                                Confirm
                            </button>
                        )

                        }

                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>

            <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
      />
        </>
    )
}