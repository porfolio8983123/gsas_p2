import * as React from 'react';
import { useState, useEffect } from 'react';
import AcademicDiversityPreview from './previewComponents/AcademicDiversityPreview'
import GenderPieChart from './previewComponents/GenderPieChart';
import BarGraphPreview from './previewComponents/BarGraphPreview';
import AllocationTable from './tables/AllocationTable';
import { useFetchAllocationQuery } from '../store';

export default function SkilingPreview({filterType, allocationId}) {
  const [filteredData, setFilteredData] = useState([]);
  const [updatedData,setUpdatedData] = useState([]);

  const skilling = [
    { academy: 'Gyalpozhing Academy', name: "ICT", MathBenchMark: "60", NoAcademiDiversity: false, data: "table", totalEnlisteeEnrolled: "2000" },
    { academy: 'Pemathang Academy', name: "CSS", EnglishBenchMark: "55", NoAcademiDiversity: false, data: "table", totalEnlisteeEnrolled: "1000" },
    { academy: 'Khotokha Academy', name: "CSS", EnglishBenchMark: "55", NoAcademiDiversity: false, data: "table", totalEnlisteeEnrolled: "1000" },
    { academy: 'Jamtsholing Academy', name: "HSS", NoAcademiDiversity: true, data: "table", totalEnlisteeEnrolled: "1000" },
    { academy: 'Jamtsholing Academy', name: "FSS", NoAcademiDiversity: true, data: "table", totalEnlisteeEnrolled: "1000" }
  ];

  const {data,isLoading,isError} = useFetchAllocationQuery(allocationId);

  useEffect(() => {
    if (data) {
      // Process skilling data
      const updatedSkilling = data?.academyStudents.map(item => {
        if (item.skillingName === "HSS" || item.skillingName === "FSS") {
          return { ...item, NoAcademiDiversity: true };
        }
        return { ...item, NoAcademiDiversity: false };
      });

      console.log("Updated skilling data: ", updatedSkilling);
      console.log("Data from API: ", data);
      setUpdatedData(updatedSkilling);
    }
  }, [data]);

  useEffect(() => {
    if (filterType === "Skillings") {
      setFilteredData(updatedData);
    } else {
      const filterData = updatedData.filter((value) => value.skillingName === filterType);
      setFilteredData(filterData);
      console.log("filterd data ",filterData);
    }
  },[filterType,updatedData])

  // Modify the MathBenchMark for Pemathang Academy and Khotokha Academy
  skilling[1].EnglishBenchMark = skilling[1].MathBenchMark;
  skilling[2].EnglishBenchMark = skilling[2].MathBenchMark;

  const findMinMarks = (students, subject) => {
    return students.reduce((min, student) => {
      const mark = student[subject];
      return mark !== null && mark !== undefined ? Math.min(min, mark) : min;
    }, Infinity);
  };

  return (
    <>
      {filteredData.map((skill, index) => (
        <div className="container mt-4 p-3" key={index}>
          <div className='d-flex justify-content-between'>
            <div>
              <h5 style={{}} className='text-start ms-5'>{skill.abbrebation} ({skill.skillingName})</h5>
              <hr className='ms-5 rounded' style={{ width: "90%", height: "3px" }}></hr>
            </div>
          </div>
          <div className='row mt-3'>
            <div className='col-md-4'>
                <p className='h6 text-center'>TotalEnlisteeEnrolled: {skill.length}</p>
            </div>
            <div className='col-md-4'>
              <p className='h6 text-center'>Total Male Enrolled: {skill.maleCount}</p>
            </div>
            <div className='col-md-4'>
              <p className='h6 text-center'>Total Female Enrolled: {skill.femaleCount}</p>
            </div>
            

          </div>
          <div className="row mt-5">
            {!skill.NoAcademiDiversity && (
              <div className="col-md-4 mb-4">
                <div>
                  <p className='text-center'>Academic Mark and Diversity Intake</p>
                </div>
                <AcademicDiversityPreview diversity = {skill.diversityIntake}/>
              </div>
            )}
            {!skill.NoAcademiDiversity && (
              <div className="col-md-4 mb-4">
                <div>
                  <p className='text-center'>Gender Intake</p>
                </div>
                <GenderPieChart totalStudent = {skill.length} maleCount = {skill.maleCount} femaleCount = {skill.femaleCount}/>
              </div>
            )}
            {!skill.NoAcademiDiversity && (
              <div className="col-md-4 mb-4 text-center">
                <div className=''>
                  <p style={{ fontSize: '20px' }} className=''>Benchmark for<br /> Academic {skill.skillingName === 'ICT' ? 'Math' : 'English'}:</p>
                </div>
                <p style={{ fontSize: '70px', color: '#F04A00' }}>{skill.skillingName === 'ICT' ? findMinMarks(skill.students,'math') : findMinMarks(skill.students,'english')}</p>
              </div>
            )}
          <div className="col-md-12 mb-12 mt-3">
            <p className='text-center'>Enlistee from each Dzongkhag</p>
              <div className='d-flex justify-content-center align-items-center'>
                <BarGraphPreview dzongkhagCount = {skill.dzongkhagCount}/>
              </div>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col-md-12 mb-12">
              <AllocationTable studentData = {skill.students}/>
            </div>
          </div>
        </div>
      ))}
    </>
  );
}
