import React from 'react';
// import { useGetAllocationByAdminQuery } from '../../store';
import { useState } from 'react';
import { useEffect } from 'react';

function OverView({userName,email}) {

  const [allAllocation,setAllAllocation] = useState([]);

  // const {data,isLoading,isError} = useGetAllocationByAdminQuery();

  // console.log("allocation overwier", data);

  // useEffect(() => {
  //   if (data) {
  //     setAllAllocation(data?.allocations);
  //   }
  // },[data])

  const formatDate = (dateString) => {
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
    };
    return new Date(dateString).toLocaleString('en-US', options);
  };

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">Profile Details</h5>
        <ul className="list-group">
          <li className="list-group-item">Name: {userName}</li>
          <li className="list-group-item">Email: {email}</li>
        </ul>
      </div>
      {/* <div className="card-body">
        <h5 className="card-title">Action ({allAllocation?.length})</h5>
        <div className="list-group" style={{overflow:"scroll",height:'150px'}}>
          {allAllocation && allAllocation.length > 0 ? (
            allAllocation.map((item) => (
              <>
                <div className="list-group-item d-flex align-items-center">
                  <h6>{item.nameOfAllocation}</h6>
                  <p style={{marginLeft:'40px',marginTop:'10px'}}>{formatDate(item.created_at)}</p>
                </div>
              </>
            ))
          ):(
            <div>
              Not Allocated yet!
            </div>
          )

          }
        </div>
      </div> */}
    </div>
  );
}

export default OverView;