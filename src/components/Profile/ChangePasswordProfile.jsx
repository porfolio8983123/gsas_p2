import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import { useChangePasswordMutation } from "../../store";
import { ToastContainer, toast } from 'react-toastify';

function ChangePasswordProfile() {
  const [showCurrentPassword, setShowCurrentPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showReEnterNewPassword, setShowReEnterNewPassword] = useState(false);

  const [currentPassword,setCurrentPassword] = useState('');
  const [password,setPassword] = useState('');
  const [confirmPassword,setConfirmPassword] = useState('');

  const [changePassword, {isLoading,isError}] = useChangePasswordMutation();

  const handleSubmit = async (e) => {
    e.preventDefault();

    const passwordData = {
      current_password:currentPassword,
      password:password,
      password_confirmation:confirmPassword
    }

    changePassword(passwordData)
      .unwrap()
      .then((response) => {
        console.log(response);
        toast.success(response.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        setConfirmPassword('');
        setCurrentPassword('');
        setPassword('');
      })
      .catch((error) => {
        console.log(error)
        toast.error(error.data.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      })
  }

  const togglePasswordVisibility = (passwordType) => {
    switch (passwordType) {
      case "current":
        setShowCurrentPassword((prevShowPassword) => !prevShowPassword);
        break;
      case "new":
        setShowNewPassword((prevShowPassword) => !prevShowPassword);
        break;
      case "reEnterNew":
        setShowReEnterNewPassword((prevShowPassword) => !prevShowPassword);
        break;
      default:
        break;
    }
  };

  console.log(currentPassword,password,confirmPassword);

  return (
    <div className="card">
      <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
      />
      <div className="card-body">
          <div className="mb-3 row">
            <label htmlFor="currentPassword" className="col-sm-4 col-form-label">
              Current Password
            </label>
            <div className="col-sm-8">
              <div className="input-group">
                <input
                  type={showCurrentPassword ? "text" : "password"}
                  className="form-control"
                  id="currentPassword"
                  onChange={(e) => setCurrentPassword(e.target.value)}
                  value={currentPassword}
                />
                <button
                  type="button"
                  className="btn btn-outline-secondary"
                  onClick={() => togglePasswordVisibility("current")}
                >
                  <FontAwesomeIcon icon={showCurrentPassword ? faEye : faEyeSlash} />
                </button>
              </div>
            </div>
          </div>
          <div className="mb-3 row">
            <label htmlFor="newPassword" className="col-sm-4 col-form-label">
              New Password
            </label>
            <div className="col-sm-8">
              <div className="input-group">
                <input
                  type={showNewPassword ? "text" : "password"}
                  className="form-control"
                  id="newPassword"
                  onChange={(e) => setPassword(e.target.value)}
                  value={password}
                />
                <button
                  type="button"
                  className="btn btn-outline-secondary"
                  onClick={() => togglePasswordVisibility("new")}
                >
                  <FontAwesomeIcon icon={showNewPassword ? faEye : faEyeSlash} />
                </button>
              </div>
            </div>
          </div>
          <div className="mb-3 row">
            <label htmlFor="reEnterNewPassword" className="col-sm-4 col-form-label">
              Re-enter New Password
            </label>
            <div className="col-sm-8">
              <div className="input-group">
                <input
                  type={showReEnterNewPassword ? "text" : "password"}
                  className="form-control"
                  id="reEnterNewPassword"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                  value={confirmPassword}
                />
                <button
                  type="button"
                  className="btn btn-outline-secondary"
                  onClick={() => togglePasswordVisibility("reEnterNew")}
                >
                  <FontAwesomeIcon icon={showReEnterNewPassword ? faEye : faEyeSlash} />
                </button>
              </div>
            </div>
          </div>
          <div className="mb-3 row">
            <div className="col-sm-12 offset-sm-4">
              <button onClick={handleSubmit} className="btn btn-block" style={{ color: "#FFFFFF", backgroundColor: "#F04A00" }}>
                Change Password
              </button>
            </div>
          </div>
      </div>
    </div>
  );
}

export default ChangePasswordProfile;