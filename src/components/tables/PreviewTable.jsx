import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useState,useEffect } from "react";

export default function PreviewTable({students}) {
//   const [isVisible, setIsVisible] = useState(false); // Initially hidden
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [searchValue, setSearchValue] = useState("");
  const [filteredRows, setFilteredRows] = useState(students); // Define filteredRows state variable

  const [columns,setColumns] = useState([]);

  console.log("student data from preview table ",students);

  useEffect(() => {
  if (students.length > 0) {
    const keys = Object.keys(students[0]);
    console.log(keys);

    setColumns(keys);
  }
  },[students])

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleSearch = (e) => {
    e.preventDefault();

    const filteredStudents = students.filter((student) => {
      const matchesCID = student.CID.toString().includes(searchValue);
      console.log('matched cid ',matchesCID);
      return matchesCID;
    });
    

    setFilteredRows(filteredStudents);
  };

  return (
    <div className="mt-2">
      <div style={{ marginBottom: "20px" }}>
        <div className="d-flex p-3">
          <input
            type="text"
            placeholder="Search..."
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
            style={{
              
              marginRight: "10px",
              width: "250px",
              
            }} // Adjusted width and height
          />
          <div>
            <button
              className="btn form-control rounded"
              style={{
                backgroundColor: "#F04A00",
                color: "white",
                marginTop: "0px",
                width: "140px", // Adjusted width
              }}
              onClick={handleSearch}
            >
              Search
            </button>
          </div>
        </div>

      </div>
    
    <Paper sx={{overflow: "hidden" }}>
        <TableContainer>
        <Table stickyHeader aria-label="sticky table">
            <TableHead>
            <TableRow>
                {columns.map((column,index) => (
                <TableCell
                    key={index}
                    align="left"
                >
                    {column}
                </TableCell>
                ))}
            </TableRow>
            </TableHead>
            <TableBody>
            {filteredRows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => (
                <TableRow key={index}>
                    {columns.map((column) => (
                    <TableCell key={column.id} align="left">
                        {typeof row[column] === "boolean" ? row[column].toString() : row[column]}
                    </TableCell>
                    ))}
                </TableRow>
                ))}
            </TableBody>
        </Table>
        </TableContainer>
        <TablePagination
        rowsPerPageOptions={[15, 25, 100]}
        component="div"
        count={filteredRows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        />
    </Paper>

    </div>
  );
}