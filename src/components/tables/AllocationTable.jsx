import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useState } from "react";

const columns = [
    { id: "CID", label: "CID", minWidth: 100 },
    { id: "name", label: "Name", minWidth: 100 },
    { id: "gender", label: "Gender", minWidth: 100 },
    { id: "dzongkhag", label: "DZONGKHAG", minWidth: 100 },
    
];

const rows = [
    {
        name: "Jane Smith",
        cid: "74927489237423",
        sex: 'M',
        dzongkhag: 'Thimphu'
    },
    {
        name: "Jane Smith",
        cid: "74927489237423",
        sex: 'M',
        dzongkhag: 'Thimphu'
    },
];

export default function AllocationTable({studentData}) {
    const [isVisible, setIsVisible] = useState(true); // Initially hidden
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [searchValue, setSearchValue] = useState("");
    const [searchCID, setSearchCID] = useState(""); // Define searchCID state variable
    const [filteredRows, setFilteredRows] = useState(studentData); // Define filteredRows state variable

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const handleSearch = (e) => {
        e.preventDefault();

        const filteredStudents = studentData.filter((student) => {
            const matchesCID = !searchValue || student.CID.toString().includes(searchValue);
            return matchesCID;
        });


        setFilteredRows(filteredStudents);
    };

    return (
        <div>

            <div className=" text-start  ps-5"
                onClick={() => setIsVisible(!isVisible)}
                style={{
                    cursor: "pointer",
                    textDecoration: "none",
                }}
            >
                {isVisible ? "Hide Enlistee..." : "Show Enlistee"}
            </div>

            {isVisible && (
                <Paper sx={{ width: "100%", overflow: "hidden" }}>
                    <div style={{ marginBottom: "20px" }}>
                        <div className="d-flex p-3">
                            <input
                                type="text"
                                placeholder="Search..."
                                value={searchValue}
                                onChange={(e) => setSearchValue(e.target.value)}
                                style={{

                                    marginRight: "10px",
                                    width: "250px",

                                }} // Adjusted width and height
                            />
                            <div

                                style={{ width: "140px", }}
                            >
                                {" "}
                                {/* Added height */}
                                <button
                                    className="btn form-control rounded"
                                    style={{
                                        backgroundColor: "#F04A00",
                                        color: "white",
                                        marginTop: "0px",
                                        width: "140px", // Adjusted width
                                        // Height set to 100% to match parent div
                                    }}
                                    onClick={handleSearch}
                                >
                                    Search
                                </button>
                            </div>
                        </div>
                    </div>
                    <TableContainer sx={{ maxHeight: 440 }}>
                        <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    {columns.map((column) => (
                                        <TableCell
                                            key={column.id}
                                            align="left"
                                            style={{ minWidth: column.minWidth }}
                                        >
                                            {column.label}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {filteredRows
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((row, index) => (
                                        <TableRow key={index}>
                                            {columns.map((column) => (
                                                <TableCell key={column.id} align="left">
                                                    {row[column.id]}
                                                </TableCell>
                                            ))}
                                        </TableRow>
                                    ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[15, 25, 100]}
                        component="div"
                        count={filteredRows.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onPageChange={handleChangePage}
                        onRowsPerPageChange={handleChangeRowsPerPage}
                    />
                </Paper>
            )}
            <hr/>
        </div>
    );
}