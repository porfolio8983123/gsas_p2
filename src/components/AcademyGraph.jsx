import React, { useEffect } from 'react';
import { ResponsiveContainer, BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Label } from 'recharts';
import { useFetchAcademyDataQuery } from '../store';

export default function AcademyGraph({allocationId}) {

  const {data:academyData,isLoading,isError} = useFetchAcademyDataQuery(allocationId);

  useEffect(() => {
    console.log("academy data ",academyData);
  },[academyData])

  const data = [
    { name: 'Thim', TotalEnlistee: 4 },
    { name: 'Paro', TotalEnlistee: 3 },
    { name: 'Haa', TotalEnlistee: 2 },
    { name: 'Tgang', TotalEnlistee: 4 },
    { name: 'Tyang', TotalEnlistee: 3 },
    { name: 'Sjong', TotalEnlistee: 2 },
    { name: 'Chuk', TotalEnlistee: 4 },
    { name: 'Trong', TotalEnlistee: 3 },
    { name: 'Wang', TotalEnlistee: 2 },
    { name: 'Puna', TotalEnlistee: 4 },
    { name: 'Gasa', TotalEnlistee: 3 },
    { name: 'Bumt', TotalEnlistee: 2 },
    { name: 'Mong', TotalEnlistee: 4 },
    { name: 'Daga', TotalEnlistee: 3 },
    { name: 'Tsir', TotalEnlistee: 2 },
    { name: 'Sarp', TotalEnlistee: 4 },
    { name: 'Zhem', TotalEnlistee: 3 },
    { name: 'Pemag', TotalEnlistee: 2 },
    { name: 'Lhue', TotalEnlistee: 4 },
    { name: 'Sam', TotalEnlistee: 3 },
  ];

  const academies = [
    { name: "Gyalpozhing Academy", TotalEnlisteeEnrolled: 1000, capacity: 1000 },
    { name: "Pemathang Academy", TotalEnlisteeEnrolled: 1000, capacity: 4500 },
    { name: "Jamtsholing Academy", TotalEnlisteeEnrolled: 1000, capacity: 5500 },
    { name: "Khotokha Academy", TotalEnlisteeEnrolled: 1000, capacity: 2000 }
  ];

  return (
    <>
      {academyData && academyData.map((academy, index) => (
        <div className='mt-5' key={index} style={{ marginBottom: '30px',  }}>
          <div className='d-flex justify-content-between'>
            <div>
              <h5 style={{ marginLeft: '' }}>{academy.name}</h5>
              <hr />
            </div>
            <h6>Capacity: {academy.capacity}</h6>
            <h6 style={{ marginRight: '80px' }}>Enlistee allocated: {academy.totalStudent}</h6>
          </div>
          <ResponsiveContainer width="90%" aspect={2.5/1.0}>
            <BarChart data={academy.students}>
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" angle={-90} textAnchor="end" interval={0} tick={{ dy: 10 }} height={60} />
              <YAxis>
                <Label value="No of TotalEnlistee" angle={-90} position="insideLeft" />
              </YAxis>
              <Tooltip />
              <Bar
                dataKey="TotalEnlistee"
                fill={index % 2 === 0 ? "rgba(240,74,0,0.8)" : "rgba(2,52,79,0.8)"}
                barSize={30}
              />
            </BarChart>
          </ResponsiveContainer>
        </div>
      ))}
    </>
  );
}
