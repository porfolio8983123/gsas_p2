import React, {useState, useEffect} from 'react';
import { useFetchAdminQuery } from '../store';

const Header = () => {

  const {data, isLoading,isError} = useFetchAdminQuery();

  const [userString,setUserString] = useState('');

  useEffect(() => {
    if (data && data.users && data.users.name) {
      let u = "";
      const user = data.users.name.split(" ");
      user.forEach(element => {
        u += element.charAt(0);
      });
      setUserString(u);
    } else {
      setUserString(''); // Or some default value
    }
  }, [data]);

  return (
    <div className='px-2 d-flex justify-content-end shadow'>
        <div className='d-flex justify-content-center align-items-center py-2'>
            <h5 className='me-4'>{data?.users.name}</h5>
            <div className='border d-flex justify-content-center align-items-center text-white' style={{width:'50px',height:'50px',borderRadius:'50%',fontWeight:'500',backgroundColor:'#F04A00'}}>{userString}</div>
        </div>
    </div>
  )
}

export default Header