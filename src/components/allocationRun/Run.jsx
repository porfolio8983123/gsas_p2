import React, { useState,useEffect } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ImDropbox } from 'react-icons/im';
import { FaCheckCircle } from 'react-icons/fa';
import { useFetchhAllAllocatedQuery, useDeleteAllocationMutation } from '../../store';

export default function Run({setNextStep, settingCompare,setAllocationName, setAllocation}) {
  const [allocations, setAllocations] = useState([]);

  const {data,isLoading,isError,refetch} = useFetchhAllAllocatedQuery();
  const [deleteAllocation, {isLoading:deleteLoading,isError:deleteError}] = useDeleteAllocationMutation();

  useEffect(() => {
    if (data) {
      // Map fetched data and add checked and isSelected properties
      console.log(data);
      const updatedAllocations = data?.data.map(item => ({
        ...item,
        checked: false,
        isSelected: false
      }));
      // Combine existing allocations with updated data
      setAllocations(prevState => [...updatedAllocations]);
    }
  }, [data]);

  const handleCheckAll = (e) => {
    const isChecked = e.target.checked;
    const updatedAllocations = allocations.map((item) => ({ ...item, checked: isChecked }));
    setAllocations(updatedAllocations);
  };

  const handleCheckboxChange = (allocationId) => {
    const updatedAllocations = allocations.map((item) =>
      item.allocationId === allocationId ? { ...item, checked: !item.checked } : item
    );
    setAllocations(updatedAllocations);
  };

  const handleDeleteSelected = () => {
    const selectedAllocations = allocations.filter((item) => item.checked);
    // Add your delete logic here
    console.log('Selected allocations to delete:', selectedAllocations);

    const allocationIds = selectedAllocations.map((item) => item.allocationId);

    console.log("all allocation id ",allocationIds);

    const deletionData = {
      allocationIds
    }

    deleteAllocation(deletionData)
    .unwrap()
    .then((response) => {
      console.log(response);
      if (response.status === 'success') {
        refetch();
      }
    })
    .catch((error) => {
      console.log("error ",error);
    })
  };

  const formatDate = (dateString) => {
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
    };
    return new Date(dateString).toLocaleString('en-US', options);
  };


  return (
      <div>
      <h5 className='mb-4 mt-4' style={{ fontWeight: 'bold' }}>All Allocation Run List</h5>
      
      <div style={{marginRight: '35px', marginLeft: '30px' }}>
        
        <div className='d-flex align-items-center justify-content-between mb-3'>
          <div>
            <input type='checkbox' onChange={handleCheckAll} />
            <label className='ms-2'>Select All</label>
          </div>

          <div className='me-3'>
            <button
              className='btn ms-5 px-4 py-1'
              style={{ backgroundColor: '#F04A00', color: 'white' }}
              onClick={() => {
                settingCompare();
              }}
            >
              Compare
            </button>
            <button
              className='btn ms-5 px-4 py-1'
              style={{ backgroundColor: '#F04A00', color: 'white' }}
              disabled={allocations.filter((item) => item.checked).length === 0}
              onClick={handleDeleteSelected}
            >
              Delete
            </button>
        </div>  

        </div>
        
        <div style={{maxHeight: 'calc(100vh - 200px)',overflowY: 'auto'}}>
          {allocations.map((item) => (
            <div key={item.id} className='container-fluid d-flex justify-content-center align-items-center mt-3 w-80 mb-2'>
              <div className='ps-2 pe-3 bg-body rounded d-flex justify-content-between align-items-center w-100 border border-dark'
                onMouseOver={(e) => (e.currentTarget.style.backgroundColor = 'red')}
                onMouseOut={(e) => (e.currentTarget.style.backgroundColor = '')}
                style={{ position: 'relative' }}
              >
                <div style={{ position: 'absolute', top: '32%', left: '2%' }}>
                  <input
                    type='checkbox'
                    checked={item.checked}
                    onChange={() => handleCheckboxChange(item.allocationId)}
                    style={{ transform: 'scale(1.5)' }}
                  />
                </div>
                <div className='d-flex align-items-center justify-content-between ms-4' style={{width:'100%'}}>
                  <div className=''>
                    <div className='d-flex align-items-center mt-3 ms-4' style={{ height: '100%' }}>
                      <div style={{cursor:'pointer'}}
                        onClick={() => {
                          setAllocation(item.allocationId);
                          setAllocationName(item.nameOfAllocation);
                          setNextStep();
                        }}
                      >
                        <p className='ms-4'>{item.nameOfAllocation}</p>
                      </div>
                      <p className='ms-5'>{formatDate(item.created_at)}</p>

                      <p className='' style={{marginLeft:'100px'}}>Sonam Tobden</p>
                    </div>
                  </div>
                  {item.isSelected === true && 
                    <div className=''>
                      <FaCheckCircle size={20} color='green'/>
                    </div>
                  }
                </div>
              </div>
            </div>
          ))}
        </div>

      
      </div>
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='light'
      />
    </div>
  );
}
