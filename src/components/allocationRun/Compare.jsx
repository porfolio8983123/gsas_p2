import React from 'react';
import ComparisonGraph from '../graphs/ComparisonGraph';
import { AiOutlineLeft } from 'react-icons/ai';

const Compare = ({settingCompareBack}) => {
  return (
    <div className=''>
        <div className='mt-3'>
            <div className="d-flex justify-content-start align-items-center"
                onClick={() => {
                    settingCompareBack();
                }}
            >
            <AiOutlineLeft size={30} style={{ alignSelf: 'center',cursor:'pointer' }} /> 
            <p className='ms-2' style={{ alignSelf: 'center', margin: 0, fontSize: '20px'}}>Comparison of 3 Run</p>
            </div>
        </div>
        <ComparisonGraph/>
    </div>
  )
}

export default Compare