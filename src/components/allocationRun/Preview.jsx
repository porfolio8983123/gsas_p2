import React, { useEffect } from 'react';
import { AiOutlineLeft } from 'react-icons/ai';
import { useFetchAllocationQuery,useUpdateAllocationMutation } from '../../store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useState } from 'react';
import Select from 'react-select';
import AcademyGraph from '../AcademyGraph';
import SkilingPreview from '../SkillingPreview';


const options = [
  {value: 0, label:"Academies"},
  {value: 1, label:"Skillings"},
  {value: 2, label:"ICT"},
  {value: 3, label:"CSS"},
  {value: 4, label:"HSS"},
  {value: 5, label:"FSS"}
]

function Preview({year,setPreviousStep,allocationName,allocationId,previewType,getAllStudentData,setNextStep}) {

  const {data,isLoading,isError} = useFetchAllocationQuery(allocationId);
  const [updateAllocation,{isLoading:updateLoading,isError:updateError}] = useUpdateAllocationMutation();

  const [selectedOption, setSelectedOption] = useState(options[0]);

  useEffect(() => {
    if (data && previewType === 'setting') {
      getAllStudentData(data.academyStudents);
    }
  },[data])

  const handleSelectChange = (seleted) => {
    setSelectedOption(seleted);
  };

  useEffect(() => {
    console.log("selected filter ",selectedOption)
  },[selectedOption])

  const customStyles = {
    control: (provided) => ({
      ...provided,
      width: 150, // Set your desired fixed width here
      textAlign: 'left',
    }),
    option: (provided, state) => ({
      ...provided,
      textAlign: 'left',
      backgroundColor: state.isFocused ? '#F04A00' : 'white',
      color: state.isFocused ? 'white' : 'black',
      '&:hover': {
        backgroundColor: '#F04A00',
        color: 'white',
      },
    }),
  };

  return (
    <div>
      <div className="d-flex justify-content-between align-items-center my-4" 
        style={{cursor:'pointer', marginRight:'35px',marginLeft:'30px'}}
      >
        {previewType && previewType === "dashboard" ? (
          <div>
            <h5>{allocationName}</h5>
          </div>
        ):(
          <div className="d-flex justify-content-center align-items-center"
            onClick={() => {
              setPreviousStep();
            }}
          >
            <AiOutlineLeft size={30} style={{ alignSelf: 'center' }}/> 
            <p className='ms-2' style={{ alignSelf: 'center', margin: 0, fontSize: '20px'}}>{allocationName}</p>
          </div>
        )

        }
        <div className="text-end me-3">
          <div className='d-flex'>
          <div className='d-flex align-items-center w-40 position-relative' style={{marginLeft:70}}>
            <div className='me-3'>
                <Select
                    options={options}
                    styles={customStyles}
                    value={selectedOption}
                    onChange={handleSelectChange}
                />
            </div>
            </div>
            {previewType && previewType === 'setting' &&
              <div className=''>
                 {updateLoading?(
                      <button className="btn px-5 me-5 arrow-btn border-danger" disabled>
                        <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                      </button>
                      ):(
                        <button className='btn px-5 me-5 arrow-btn border-danger' 
                          onClick={(e) => {
                            e.stopPropagation();
                            updateAllocation(allocationId)
                            .unwrap()
                            .then((response) => {
                              console.log(response);
                              toast.success(`${response.message}`, {
                                position: "top-right",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                                theme: "light",
                                });
                              setTimeout(() => {
                                setNextStep();
                              },2000)
                            })
                            .catch((error) => {
                              console.log(error)
                              toast.error(error, {
                                position: "top-right",
                                autoClose: 5000,
                                hideProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                draggable: true,
                                progress: undefined,
                                theme: "light",
                                });
                            })
                          }}
                        >Save</button>
                        )

                      }
              </div>
            }
          </div>
          </div>
        </div>
      <div
      style={{
        maxHeight: 'calc(100vh - 200px)',
        overflowY: 'scroll',
        marginRight: '35px',
        marginLeft: '30px',
        scrollbarWidth: 'thin',
      }}>
        {/* <Main data = {data}/> */}
       {selectedOption.value === 0 ? (
        <AcademyGraph allocationId = {allocationId}/>
       ):(
        <>
         <SkilingPreview filterType = {selectedOption.label} allocationId = {allocationId}/>
        </>
       )

       }
      </div>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        />
        {/* Same as */}
      <ToastContainer />
    </div>
  )
}

export default Preview
