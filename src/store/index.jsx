import { configureStore } from "@reduxjs/toolkit";
import { updateSideLine, sideLineReducer } from "./slices/sideLineSlice";
import { setupListeners } from "@reduxjs/toolkit/query";
import { academyApi } from "./apis/academyApi";
import { fileApi } from "./apis/fileApi";
import { skillingApi } from "./apis/skillingApi";
import {fileReducer,updateData,addStudentData} from './slices/fileSlices';
import { enlisteeApi } from "./apis/enlisteeApi";
import { academySkillingApi } from "./apis/AcademySkillingApi";
import { allocationApi } from "./apis/allocationApi";
import { adminApi } from "./apis/adminApi";
import { profileApi } from "./apis/profileApi";
import { skillingAllocationApi } from "./apis/skillingAllocationApi";
import { configurationReducer,updateAcademyMark,updateFemaleCount,updateSkilling } from "./slices/configurationSlice";
import { trackerReducer,updateAllocationId,updateIntakeId } from "./slices/trackerSlice";

export const store=configureStore({
    reducer: {
        sideLine:sideLineReducer,
        [academyApi.reducerPath]:academyApi.reducer,
        [fileApi.reducerPath]:fileApi.reducer,
        [skillingApi.reducerPath]:skillingApi.reducer,
        file:fileReducer,
        [enlisteeApi.reducerPath]:enlisteeApi.reducer,
        [academySkillingApi.reducerPath]:academySkillingApi.reducer,
        [allocationApi.reducerPath]:allocationApi.reducer,
        [adminApi.reducerPath]:adminApi.reducer,
        [profileApi.reducerPath]:profileApi.reducer,
        configuration: configurationReducer,
        tracker: trackerReducer,
        [skillingAllocationApi.reducerPath]:skillingAllocationApi.reducer,
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware()
            .concat(fileApi.middleware)
            .concat(academyApi.middleware)
            .concat(skillingApi.middleware)
            .concat(academySkillingApi.middleware)
            .concat(allocationApi.middleware)
            .concat(adminApi.middleware)
            .concat(profileApi.middleware)
            .concat(enlisteeApi.middleware)
            .concat(skillingAllocationApi.middleware)
    }
})

setupListeners(store.dispatch)

export {useFetchAcademiesQuery,useAddAcademyMutation,useUpdateAcademyMutation,useUpdateAcademyStatusMutation,useFetchSingleAcademyQuery} from './apis/academyApi';
export {useCheckFileNameMutation,useDeleteFileMutation,useFetchFilesQuery,useGetDzongkhagQuery} from './apis/fileApi';
export {useAddSkillingMutation,useDeleteSkillingMutation,useFetchSkillingsQuery,useUpdateSkillingMutation} from './apis/skillingApi';
export {useAddStudentMutation} from './apis/enlisteeApi';
export {useFetchAcademySkillingQuery,useFetchSingleAcademySkillingQuery} from './apis/AcademySkillingApi';
export {useStartAllocationMutation,useFetchhAllAllocatedQuery,useFetchAllocationQuery,useFetchAllocatedQuery,useDeleteAllocationMutation,useUpdateAllocationMutation,useGetSelectedAllocationQuery,useGetAllocationByAdminQuery,useGetMaleAndFemaleCountQuery,useFetchAcademyDataQuery} from './apis/allocationApi';
export {useFetchAllAdminQuery} from './apis/adminApi';
export {useChangePasswordMutation,useFetchAdminQuery} from './apis/profileApi';
export {useIctAllocationMutation,useCssAllocationMutation, useFssAllocationMutation,useHssAllocationMutation} from './apis/skillingAllocationApi';

export {
    updateSideLine,
    updateData,
    addStudentData,
    updateAcademyMark,
    updateFemaleCount,
    updateSkilling,
    updateAllocationId,
    updateIntakeId
}