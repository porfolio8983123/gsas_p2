import { createSlice } from "@reduxjs/toolkit";

const trackerSlice = createSlice({
    name: "tracker",
    initialState: {
        allocationId:0,
        intakeId:0
    },
    reducers: {
        updateAllocationId: (state,action) => {
            state.allocationId = action.payload;
        },
        updateIntakeId: (state,action) => {
            state.intakeId = action.payload;
        }
    }
})

export const {updateAllocationId,updateIntakeId} = trackerSlice.actions;
export const trackerReducer = trackerSlice.reducer;