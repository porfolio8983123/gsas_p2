import { createSlice } from "@reduxjs/toolkit";

const configurationSlice = createSlice({
    name: 'configuration',
    initialState: {
        academyMark:0,
        femaleCount: 500,
        skilling:''
    },
    reducers: {
        updateAcademyMark: (state,action) => {
            state.academyMark = action.payload;
        },
        updateFemaleCount: (state,action) => {
            state.femaleCount = action.payload;
        },
        updateSkilling: (state,action) => {
            state.skilling = action.payload;
        }
    }
})

export const {updateAcademyMark,updateFemaleCount,updateSkilling} = configurationSlice.actions;
export const configurationReducer = configurationSlice.reducer;