import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const enlisteeApi = createApi({
    reducerPath: 'enlistee',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization',`Bearer ${token}`);
            return headers;
        }
    }),
    endpoints(builder) {
        return {
            addStudent: builder.mutation({
                query: (data) => ({
                    url: 'enlistee',
                    method:'POST',
                    body: data
                })
            })
        }
    }
})

export const {useAddStudentMutation} = enlisteeApi;
export {enlisteeApi};