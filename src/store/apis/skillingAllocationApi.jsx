import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import Cookies from 'js-cookie';

const skillingAllocationApi = createApi({
    reducerPath:'skillingAllocation',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization',`Bearer ${token}`);
            return headers;
        }
    }),
    endpoints(builder) {
        return {
            ictAllocation: builder.mutation({
                query:(allocationData) => ({
                    url:'ictAllocation',
                    method:'POST',
                    body: allocationData
                })
            }),
            cssAllocation: builder.mutation({
                query:(allocationData) => ({
                    url:'cssAllocation',
                    method:'POST',
                    body: allocationData
                })
            }),
            hssAllocation: builder.mutation({
                query:(allocationData) => ({
                    url:'hssAllocation',
                    method:'POST',
                    body: allocationData
                })
            }),
            fssAllocation: builder.mutation({
                query:(allocationData) => ({
                    url:'fssAllocation',
                    method:'POST',
                    body: allocationData
                })
            })
        }
    }
})

export const {useIctAllocationMutation, useCssAllocationMutation,useHssAllocationMutation, useFssAllocationMutation} = skillingAllocationApi;
export {skillingAllocationApi};