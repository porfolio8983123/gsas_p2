import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const allocationApi = createApi({
    reducerPath:'allocation',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization',`Bearer ${token}`);
            return headers;
        }
    }),
    endpoints(builder) {
        return {
            startAllocation: builder.mutation({
                query:(allocationData) => ({
                    url: 'startAllocation',
                    method:'POST',
                    body: allocationData,
                })
            }),
            fetchAllocated: builder.query({
                query: (allocationId) => {
                    return{
                        url: `getAllCurrentAllocation/${allocationId}`,
                        method: 'GET'
                    }
                }
            }),
            fetchhAllAllocated: builder.query({
                query: () => {
                    return {
                        url: 'getAllAllocation',
                        method: 'GET'
                    }
                }
            }),
            fetchAllocation: builder.query({
                query: (allocationId) => {
                    return {
                        url: `getAllocation/${allocationId}`,
                        method: 'GET'
                    }
                }
            }),
            fetchAcademyData: builder.query({
                query: (allocationId) => {
                    return {
                        url: `getAcademyData/${allocationId}`,
                        method: 'GET'
                    }
                }
            }),
            deleteAllocation: builder.mutation({
                query: (deletionData) => ({
                    url: `deleteAllocation`,
                    method: 'POST',
                    body: deletionData
                })
            }),
            updateAllocation: builder.mutation({
                query: (allocationId) => ({
                    url: `saveAllocation/${allocationId}`,
                    method: 'PUT'
                })
            }),
            getSelectedAllocation: builder.query({
                query: () => ({
                    url: 'getSelectedAllocation',
                    method: 'GET'
                })
            }),
            getAllocationByAdmin: builder.query({
                query: () => ({
                    url:'getAllocationByAdmin',
                    method:'GET'
                })
            }),
            getMaleAndFemaleCount: builder.query({
                query: () => ({
                    url: 'getSelectedMaleAndFemaleCount',
                    method:'GET'
                })
            })
        }
    }
})

export const {useStartAllocationMutation, useFetchhAllAllocatedQuery,useFetchAllocationQuery,useFetchAllocatedQuery,useDeleteAllocationMutation,useUpdateAllocationMutation,useGetSelectedAllocationQuery,useGetAllocationByAdminQuery,useGetMaleAndFemaleCountQuery, useFetchAcademyDataQuery} = allocationApi;
export {allocationApi};