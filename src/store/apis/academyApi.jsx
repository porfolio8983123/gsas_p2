import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const academyApi = createApi({
    reducerPath:'academy',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchAcademies: builder.query({
                query: () => {
                    return {
                        url: '/academy',
                        method: 'GET'
                    }
                }
            }),
            addAcademy: builder.mutation({
                query: (data) => ({
                    url:'/academy',
                    method:'POST',
                    body:data
                })
            }),
            updateAcademy: builder.mutation({
                query: ({id,data}) => ({
                    url:`/academy/${id}`,
                    method: 'PUT',
                    body: data
                })
            }),
            updateAcademyStatus: builder.mutation({
                query: ({id,data}) => ({
                    url: `/academy/${id}/updateStatus`,
                    method:'PATCH',
                    body:data
                })
            }),
            fetchSingleAcademy: builder.query({
                query: (id) => {
                    return {
                        url: `getAcademy/${id}`,
                        method: 'GET'
                    }
                }
            })
        }
    }
})

export const {useFetchAcademiesQuery,useAddAcademyMutation,useUpdateAcademyMutation,useUpdateAcademyStatusMutation, useFetchSingleAcademyQuery} = academyApi;
export {academyApi};