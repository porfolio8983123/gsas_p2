import React, { useEffect } from 'react';
import AllAcademy from '../components/AllAcademy';
import AddAcademy from '../components/AddAcademy';
import { useState } from 'react';
import EditAcademy from '../components/EditAcademy';
import { useFetchAcademiesQuery } from '../store';
import Header from '../components/Header';

export default function Academy() {

  const [isAddAcademy,setIsAddAcademy] = useState(false);
  const [isEditAcademy,setIsEditAcademy] = useState(false);
  const [editingAcademy,setEditingAcademy] = useState([]);

  const { data, error, isLoading,refetch } = useFetchAcademiesQuery();
  console.log("academy data ", data, error, isLoading);

  useEffect(() => {
    refetch();
  },[isAddAcademy,isEditAcademy])

  const getAcademy = () => {
    console.log("getting all academy again");
    refetch();
  }

  function setAddAcademy(){
    setIsAddAcademy(true);
    setIsEditAcademy(false);
  }

  function setEditAcademy(academy){
    setEditingAcademy(academy);
    setIsAddAcademy(false);
    setIsEditAcademy(true);
  }

  function backToAcademy(){
    setIsAddAcademy(false);
    setIsEditAcademy(false);
  }

  console.log("editing academy from academy ",editingAcademy);

  return (
    <>
      <Header />
      {!isAddAcademy && !isEditAcademy &&
        <AllAcademy setAddAcademy={setAddAcademy} setEditAcademy={setEditAcademy} data = {data?.academies} getAcademy = {getAcademy}/>
      }
      {isAddAcademy && !isEditAcademy &&
        <AddAcademy backToAcademy={backToAcademy} getAcademy = {getAcademy}/>
      }

      {isEditAcademy && !isAddAcademy &&
        <EditAcademy backToAcademy={backToAcademy} academy = {editingAcademy} getAcademy = {getAcademy}/>
      }
    </>
  )
}
