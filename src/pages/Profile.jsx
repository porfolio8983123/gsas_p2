import React, { useState } from "react";
import ChangePasswordProfile from "../components/Profile/ChangePasswordProfile";
import OverView from "../components/Profile/Overview";
import Header from "../components/Header";
import { useFetchAdminQuery } from "../store";
import { useEffect } from "react";

function Profile() {
  const [showProfile, setShowProfile] = useState(true);
  const [userName,setUserName] = useState('');
  const [email,setEmail] = useState('');
  const [userString,setUserString] = useState('');

  const {data,isLoading,isError} = useFetchAdminQuery();

  useEffect(() => {
    console.log("from profile ", data?.users);
    if (data) {
      setUserName(data?.users.name)
      setEmail(data?.users.email);
    }
  },[data])

  useEffect(() => {
    let u = "";
    const user = userName.split(" ");
    user.forEach(element => {
      u+=element.charAt(0);
    });

    setUserString(u);
  },[userName])

  const handleTabClick = (tab) => {
    if (tab === "profile") {
      setShowProfile(true);
    } else if (tab === "changePassword") {
      setShowProfile(false);
    }
  };

  return (
    <div className="">
      <Header />

      <div
        className="row d-flex"
        style={{
          height: "500px",
          width: "95%",
          boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
          marginLeft:'25px',
          marginTop:'30px'
        }}
      >
        <div className="col-md-4 d-flex justify-content-center align-items-center" style={{ marginTop: "100px" }}>
          <div
            className="col-md-12 border border-dark"
            style={{
              display: "flex",
              width: "200px",
              height: "200px",
              borderRadius: "50%",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#2C5F7C",
              color: "#FFFFFF",
            }}
          >
            <h3 style={{ fontSize: "100px" }}>{userString}</h3>
          </div>
        </div>

        <div className="col-md-8">
          <div className="row d-flex justify-content-center">
            <div className="col-md-6 d-flex justify-content-center" style={{ marginTop: "50px" }}>
              <a
                href="#profile"
                className="text-decoration-none"
                onClick={(e) => {
                  e.preventDefault();
                  handleTabClick("profile");
                }}
                style={{paddingLeft:'10px',paddingRight:'10px', borderBottom:showProfile?'4px solid #F04A00':'none',paddingBottom:'10px',fontSize:'20px',color:'black'}}
              >
                Profile Detail
              </a>
            </div>
            <div className="col-md-6 d-flex justify-content-center" style={{ marginTop: "50px" }}>
              <a
                href="#changePassword"
                className="text-decoration-none"
                onClick={(e) => {
                  e.preventDefault();
                  handleTabClick("changePassword");
                }}
                style={{paddingLeft:'10px',paddingRight:'10px', borderBottom:!showProfile?'4px solid #F04A00':'none',paddingBottom:'10px',fontSize:'20px',color:'black'}}
              >
                Change Password
              </a>
            </div>
          </div>
          <div className="row mt-2">
            <div className="col-md-12">
              <hr className={`line ${showProfile ? "left" : "right"}`} />
              {showProfile ? <OverView userName = {userName} email = {email}/> : <ChangePasswordProfile />}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Profile;
