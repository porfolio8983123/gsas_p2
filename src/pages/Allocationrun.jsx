import React, { useEffect, useState } from 'react'
import Header from '../components/Header';
import Run from '../components/allocationRun/Run';
import Preview from '../components/allocationRun/Preview';
import Compare from '../components/allocationRun/Compare';

export default function Allocationrun() {

  const [activeStep, setActiveStep] = useState(0);
  const [allocationId,setAllocationId] = useState(null);
  const [allocaitonName,setAllocaitonName] = useState(null);
  const [compare,setCompare] = useState(0);

  const setNextStep = () => {
    setActiveStep(activeStep + 1);
  }

  const setPreviousStep = () => {
    setActiveStep(activeStep - 1);
  }

  const setAllocation = (id) => {
    setAllocationId(id);
  }

  const setAllocationName = (name) => {
    setAllocaitonName(name);
  }

  const settingCompare = () => {
    setActiveStep(2)
  }

  const settingCompareBack = () => {
    setActiveStep(0)
  }

  function getSectionComponent() {
    switch (activeStep) {
      case 0:
        return <Run
          setNextStep = {setNextStep}
          setPreviousStep = {setPreviousStep}
          setAllocation = {setAllocation}
          setAllocationName = {setAllocationName}
          settingCompare = {settingCompare}
        />;
      case 1:
        return <Preview 
                setPreviousStep = {setPreviousStep}
                allocationName={allocaitonName}
                allocationId = {allocationId}
                previewType="allocationrun"
            />
      case 2:
        return <Compare 
          settingCompareBack = {settingCompareBack}
        />
      default:
        return null;
    }
  }

  useEffect(() => {
    console.log("current allocation ",allocationId)
  },[allocationId])

  useEffect(() => {
    if (activeStep === 0 || activeStep === 1) {
      // Apply the 'no-scroll' class to the body to disable scrolling
      document.body.classList.add('no-scroll');
    } else {
      // Remove the 'no-scroll' class to enable scrolling
      document.body.classList.remove('no-scroll');
    }
  }, [activeStep]);

  return (
    <div>
      <Header/>

      <div>
        {getSectionComponent()}
      </div>
    </div>
  )
}
