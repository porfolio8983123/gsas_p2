import React, { useEffect, useState } from 'react';
import Header from '../components/Header';
import Enlisteedetail from '../components/Enlisteedetail';
import Preview from '../components/allocationRun/Preview';
import { useGetSelectedAllocationQuery } from '../store';

const Dashboard = () => {
  const { data, isLoading, isError } = useGetSelectedAllocationQuery();

  const [maleCount, setMaleCount] = useState(0);
  const [femaleCount, setFemaleCount] = useState(0);

  const [close,setClose] = useState(false);

  useEffect(() => {

    console.log("data from dashboard ",data);

    if (data?.allocation?.length) {
      console.log("data from dashboard ", data.allocation[0]);
      let male = 0;
      let female = 0;
      data.allocation[0].forEach((item) => {
        console.log("item male ", item.maleCount);
        male += parseInt(item.maleCount, 10);
        female += parseInt(item.femaleCount, 10);
      });

      console.log("total male ", male);
      console.log("female count ", female);

      setFemaleCount(female);
      setMaleCount(male);
    }
  }, [data]);


  return (
    <>
      <Header />
      <p style={{position:'absolute',top:'20px',fontWeight:'bold',marginLeft:'10px',cursor:'pointer'}} onClick={() => {
        setClose(!close);
      }}>Toggle</p>
      {close &&
        <Enlisteedetail male={maleCount} female={femaleCount} />
      }

      {isLoading &&
        <div>Loading...</div>
      }

      {data?.allocation?.length && data.allocation[0].length ? (
        <Preview
          allocationName={data.allocation[0][0].nameOfAllocation}
          allocationId={data.allocation[0][0].allocationId}
          previewType="dashboard"
        />
      ) : (
        <div>No allocation data available.</div>
      )}
    </>
  );
};

export default Dashboard;
