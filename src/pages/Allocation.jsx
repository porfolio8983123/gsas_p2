import React, { useState,useEffect } from 'react'
import { Stepper } from 'react-form-stepper';
import SelectFile from '../components/allocation/SelectFile';
import Preview from '../components/allocation/Preview';
import Setting from '../components/allocation/Setting';
// import PreviewResult from '../components/allocation/PreviewResult';
import Output from '../components/allocation/Output';
import Header from '../components/Header';
import PreviewOther from '../components/allocationRun/Preview';

export default function Allocation() {

  const [activeStep,setActiveStep] = useState(0);
  const [allocationId,setAllocationId] = useState(null);
  const [allocationName,setAllocationName] = useState(null);
  const [currentAllocations, setCurrentAllocations] = useState([]);
  const [allStudentData,setAllStudentData] = useState([]);

  const steps = [
    { label: 'Select' },
    { label: 'Preview' },
    {label:'Setting'},
    {label:'Preview Result'},
    {label:'Output'}
  ];

  const getAllStudentData = (student) => {
    setAllStudentData(student);
  }

  const setNextStep = () => {
    setActiveStep(activeStep + 1);
  }

  const setPreviousStep = () => {
    setActiveStep(activeStep - 1);
  }

  const setAllocation = (id) => {
    setAllocationId(id);
  }

  const setName = (name) => {
    setAllocationName(name);
  }

  function getSectionComponent() {
    switch (activeStep) {
      case 0:
        return <SelectFile
          setNextStep = {setNextStep}
          setPreviousStep = {setPreviousStep}
        />;
      case 1:
        return <Preview 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
        />;
      case 2:
        return <Setting 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
          setAllocation = {setAllocation}
          setName = {setName}
          currentAllocations = {currentAllocations}
          setCurrentAllocations = {setCurrentAllocations}
        />;
      case 3:
        return <PreviewOther 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
          allocationName={allocationName}
          allocationId={allocationId}
          previewType="setting"
          getAllStudentData = {getAllStudentData}
        />;
      case 4:
        return <Output 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
          allStudentData = {allStudentData}
          type = "setting"
        />;
      default:
        return null;
    }
  }

  useEffect(() => {
    if (activeStep === 3 || activeStep === 2 || activeStep === 1 || activeStep === 4) {
      // Apply the 'no-scroll' class to the body to disable scrolling
      document.body.classList.add('no-scroll');
    } else {
      // Remove the 'no-scroll' class to enable scrolling
      document.body.classList.remove('no-scroll');
    }
  }, [activeStep]);

  console.log("from allocation allocation id an dname ",allocationId,allocationName)

  return (
    <div>
      <div className=''>
        <Header/>
      </div>
      <div style={{ padding: '0 10px 10px 10px'}}>
      <div className='mb-5' style={{marginTop:'100px'}}>
          <Stepper
            steps={steps}
            activeStep={activeStep}
            className="sticky-stepper"
            style={{
              position: 'fixed',
              top: '80px',
              left: '267px', // Use the desired left value in pixels
              right: '10px', // Use the desired right value in pixels
              zIndex: 1
            }}
          />
        </div>
        <div className='mt-5' style={{ padding: '20px'
        }}>
          {getSectionComponent()}
        </div>
      </div>
    </div>
  )
}
