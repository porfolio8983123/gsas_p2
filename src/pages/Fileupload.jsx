import React, { useState,useEffect } from 'react';
import Header from './../components/Header';
import FileUpload from '../components/fileUpload/File';
import Preview from '../components/fileUpload/Preview';
import { Stepper } from 'react-form-stepper';



export default function Fileupload() {
  const [activeStep, setActiveStep] = useState(0);
  const [selectedFile, setSelectedFile] = useState(null);
  const [fileName,setFileName] = useState('');

  const steps = [
    { label: 'Upload' },
    { label: 'Preview' }
  ];

  const handleFileSelect = (file) => {
    setSelectedFile(file);
  }

  const setNextStep = () => {
    setActiveStep(activeStep + 1);
  }

  const setPreviousStep = () => {
    setActiveStep(activeStep - 1);
  }

  const setFileUploadedName = (filename) => {
    setFileName(filename)
  }

  const clearInput = () => {
    setFileName('')
    setSelectedFile(null);
  }

  function getSectionComponent() {
    switch (activeStep) {
      case 0:
        return <FileUpload selectedFile = {selectedFile} onFileSelect = {handleFileSelect}
          setNextStep = {setNextStep}
          setPreviousStep = {setPreviousStep}
          setFileUploadedName = {setFileUploadedName}
          fileName = {fileName}
          clearInput = {clearInput}
        />;
      case 1:
        return <Preview 
          setPreviousStep = {setPreviousStep}
          selectedFile = {selectedFile}
          fileName = {fileName}
          setActiveStep = {setActiveStep}
        />;
      default:
        return null;
    }
  }
  
    useEffect(() => {
      if (activeStep === 1) {
        // Apply the 'no-scroll' class to the body to disable scrolling
        document.body.classList.add('no-scroll');
      } else {
        // Remove the 'no-scroll' class to enable scrolling
        document.body.classList.remove('no-scroll');
      }
    }, [activeStep]);

  return (
    <div>
      <div className=''>
        <Header/>
      </div>
      <div style={{ padding: '0 10px 10px 10px'}}>
      <div style={{marginTop:'100px'}}>
          <Stepper
            steps={steps}
            activeStep={activeStep}
            className="sticky-stepper"
            style={{
              position: 'fixed',
              top: '80px',
              left: '267px', // Use the desired left value in pixels
              right: '10px', // Use the desired right value in pixels
              zIndex: 1
            }}
          />
        </div>
        <div style={{ padding: '20px' }}>
          {getSectionComponent()}
        </div>
      </div>
    </div>
  );
}
